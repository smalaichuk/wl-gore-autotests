const { join } = require('path');
const fs = require('fs');
const filesUtil = require("./src/support/filesUtil");
const argv = require("yargs").argv;
const wdioParallel = require('wdio-cucumber-parallel-execution');

global.debugInfo = join(__dirname, './tests/debugInfo');
let sourceSpecsDirectory = './src/features/**/*.feature';
// If parallel execution is set to true, then create the Split the feature files
// And store then in a tmp spec directory (created inside `the source spec directory)
if (argv.parallel === 'true') {
    //parse from FEATURE specs path and feature name
    var features = process.env.FEATURE;
    const re = /\/(\w+|\*).feature/;
    const matchesArray = re.exec(features); 
    const sourceSpecDirectory = `./${features.replace(`/${matchesArray[1]}`, "").replace(".feature", "")}`;
    
    const fetfile = matchesArray[1].replace('.feature', "").replace('/', "");

    sourceSpecsDirectory = `${sourceSpecDirectory}/tmp/*.feature`;
    
    tmpSpecDirectory = `${sourceSpecDirectory}/tmp`;
    tmpFolderForScenarios = tmpSpecDirectory;
    if(fetfile === '*')
    {
        wdioParallel.performSetup({
            sourceSpecDirectory: sourceSpecDirectory,
            tmpSpecDirectory: tmpSpecDirectory,
            cleanTmpSpecDirectory: true
        });
    }
    else{
        wdioParallel.performSetup({
            sourceSpecDirectory: sourceSpecDirectory,
            tmpSpecDirectory: tmpSpecDirectory,
            ff: fetfile,
            cleanTmpSpecDirectory: true
        });
    }

}

exports.config = {
    windowSize: {
        width: 1200,
        height: 910,
    },
    appName: 'Default Gore app',
    //
    //
    // ====================
    // Runner Configuration
    // ====================
    //
    // eslint-disable-next-line max-len
    // WebdriverIO allows it to run your tests in arbitrary locations (e.g. locally or
    // on a remote machine).
    // runner: 'local',
    runner: 'local',
    //
    // Override default path ('/wd/hub') for chromedriver service.
    // path: '/',

    // hostname: '127.0.0.1',
    // path: '/wd/hub',
    // port: 4444,
    //
    // hostname: "http://viktar.silakou%40globant.com:u0021eeb6b02555a@hub.crossbrowsertesting.com",
    // port: 4444,
    // path: '/wd/hub',
    //
    // ==================
    // Specify Test Files
    // ==================
    // Define which test specs should run. The pattern is relative to the
    // directory from which `wdio` was called. Notice that, if you are calling
    // `wdio` from an NPM script (see https://docs.npmjs.com/cli/run-script)
    // then the current working directory is where your package.json resides, so
    // `wdio` will be called from there.
    //
    specs: [
        `${sourceSpecsDirectory}`,
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    //
    // ============
    // Capabilities
    // ============
    // Define your capabilities here. WebdriverIO can run multiple capabilities
    // at the same time. Depending on the number of capabilities, WebdriverIO
    // launches several test sessions. Within your capabilities you can
    // overwrite the spec and exclude options in order to group specific specs
    // to a specific capability.
    //
    // First, you can define how many instances should be started at the same
    // time. Let's say you have 3 different capabilities (Chrome, Firefox, and
    // Safari) and you have set maxInstances to 1; wdio will spawn 3 processes.
    // Therefore, if you have 10 spec files and you set maxInstances to 10, all
    // spec files will get tested at the same time and 30 processes will get
    // spawned. The property handles how many capabilities from the same test
    // should run tests.
    //
    maxInstances: 3,
    maxInstancesPerCapability: 3,
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // Level of logging verbosity: silent | verbose | command | data | result |
    // error
    logLevel: 'warn',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten getUrl command calls. If your getUrl
    // parameter starts with "/", then the base getUrl gets prepended.
    // baseUrl: 'http://localhost:8088',
    baseUrl: '/',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 10000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    //
    // Initialize the browser instance with a WebdriverIO plugin. The object
    // should have the plugin name as key and the desired plugin options as
    // properties. Make sure you have the plugin installed before running any
    // tests. The following plugins are currently available:
    // WebdriverCSS: https://github.com/webdriverio/webdrivercss
    // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
    // Browserevent: https://github.com/webdriverio/browserevent
    // plugins: {
    //     webdrivercss: {
    //         screenshotRoot: 'my-shots',
    //         failedComparisonsRoot: 'diffs',
    //         misMatchTolerance: 0.05,
    //         screenWidth: [320,480,640,1024]
    //     },
    //     webdriverrtc: {},
    //     browserevent: {}
    // },
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They
    // enhance your test setup with almost no effort. Unlike plugins, they don't
    // add new commands. Instead, they hook themselves up into the test process.
    //
    //
    // Test runner services
    // eslint-disable-next-line max-len
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    services:
        [
            ['image-comparison',
                // The options
                {
                    baselineFolder: join(process.cwd(), './tests/BaselineImages/'),
                    formatImageName: '{tag}-{browserName}-{platformName}-{width}x{height}',
                    screenshotPath: join(process.cwd(), '.tmp/'),
                    savePerInstance: true,
                    autoSaveBaseline: true,
                    clearRuntimeFolder: true,
                    blockOutStatusBar: true,
                    blockOutToolBar: true
                },
            ],
            ['shared-store']
        ],

    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html
    //
    // Make sure you have the wdio adapter package for the specific framework
    // installed before running any tests.
    framework: 'cucumber',
    // mochaOpts: {
    //     timeout: 5000,
    // },
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/testrunner/reporters.html
    // reporters: ['spec'],

    reporters: ['spec',
        ['allure', {
            outputDir: 'tests/reports',
            disableWebdriverStepsReporting: true,
            disableWebdriverScreenshotsReporting: true,
            useCucumberStepReporter: true,
        }],
    ],

    //
    // If you are using Cucumber you need to specify the location of your step
    // definitions.
    cucumberOpts: {
        // <boolean> show full backtrace for errors
        backtrace: false,
        // <string[]> module used for processing required features
        requireModule: ['@babel/register'],
        // <boolean< Treat ambiguous definitions as errors
        failAmbiguousDefinitions: true,
        // <boolean> invoke formatters without executing steps
        // dryRun: false,
        // <boolean> abort the run on first failure
        failFast: false,
        // <boolean> Enable this config to treat undefined definitions as
        // warnings
        ignoreUndefinedDefinitions: false,
        // <string[]> ("extension:module") require files with the given
        // EXTENSION after requiring MODULE (repeatable)
        name: [],
        // <boolean> hide step definition snippets for pending steps
        snippets: true,
        // <boolean> hide source uris
        source: true,
        // <string[]> (name) specify the profile to use
        profile: [],
        // <string[]> (file/dir) require files before executing features
        require: [
            './src/support/world.js',
            './src/support/hooks.js',
            './src/steps/given.js',
            './src/steps/then.js',
            './src/steps/when.js',
            // './src/steps/goreMedCom/*',
            // './test/world.js',
            // Or search a (sub)folder for JS files with a wildcard
            // works since version 1.1 of the wdio-cucumber-framework
            // './src/**/*.js',
        ],
        // <string> specify a custom snippet syntax
        snippetSyntax: undefined,
        // <boolean> fail if there are any undefined or pending steps
        strict: true,
        // <string> (expression) only execute the features or scenarios with
        // tags matching the expression, see
        // https://docs.cucumber.io/tag-expressions/
        tagExpression: 'not @Pending',
        // <boolean> add cucumber tags to feature or scenario name
        tagsInTitle: false,
        // <number> timeout for step definitions
        timeout: 90000,
    },

    //
    // =====
    // Hooks
    // =====
    // WebdriverIO provides several hooks you can use to interfere with the test
    // process in order to enhance it and to build services around it. You can
    // either apply a single function or an array of methods to it. If one of
    // them returns with a promise, WebdriverIO will wait until that promise got
    // resolved to continue.
    //
    // Gets executed once before all workers get launched.
    onPrepare: function (config, capabilities) {
        // make sure download directory exists
        if (!fs.existsSync(downloadDir)){
            // if it doesn't exist, create it
            fs.mkdirSync(downloadDir);
        }
        if (!fs.existsSync(debugInfo)){
            // if it doesn't exist, create it
            fs.mkdirSync(debugInfo);
        }
        if (fs.existsSync(join(process.cwd(), '.tmp/'))){       
            filesUtil.rmdir(join(process.cwd(), '.tmp/'));
        }
        // if(fs.existsSync(tmpFolderForScenarios)){
        //     filesUtil.rmdir(tmpFolderForScenarios);
        // }
      },
    
    // Gets executed before test execution begins. At this point you can access
    // all global variables, such as `browser`. It is the perfect place to
    // define custom commands.
    before: function before(scenario) {
        /**
         * Setup the Chai assertion framework
         */
        const chai = require('chai');
        global.expect = chai.expect;
        global.be = chai.be;
        global.assert = chai.assert;
        global.should = chai.should();
        global.feedbackArea = false;
    },


    //
    // Hook that gets executed before the suite starts
    // beforeSuite: function beforeSuite(suite) {
    // },

    //
    // Hook that gets executed _before_ a hook within the suite starts (e.g.
    // runs before calling beforeEach in Mocha)
    // beforeHook: function beforeHook() {
    // },
    //
    // Hook that gets executed _after_ a hook within the suite starts (e.g. runs
    // after calling afterEach in Mocha)
    //
    // beforeSession: function beforeTest() {
    // },
    //
    // Runs before a WebdriverIO command gets executed.
    // beforeCommand: function beforeCommand(commandName, args) {
    // },
    //
    // Runs after a WebdriverIO command gets executed
    // afterCommand: function afterCommand(commandName, args, result, error) {
    // },
    //
    // Function to be executed after a test (in Mocha/Jasmine) or a step (in
    // Cucumber) starts.
    beforeScenario: async function (uri, feature, scenario, sourceLocation) {
        /*
        * Initialize the VRT Driver
        * */
        // if (scenario.tags.filter(it => it.name === '@visual').length > 0) {
        //     browser.vDriver = new this.VRTDriver();
        //     // browser.config.vDriver = new this.VRTDriver();
        //     await browser.vDriver.init({});
        //     // by default hook sets the suite/test name and the id as a feature/scenario name, you can change this behaviour inside steps
        //     browser.vDriver.suite = {
        //         name: feature.document.feature.name,
        //         id: feature.document.feature.name
        //     };


        //     await browser.vDriver.startSession(browser, {
        //         app: this.appName,
        //         test: scenario.name
        //     });
        // }
    },

    afterScenario: async function afterScenario(uri, feature, scenario, sourceLocation) {
        // if (scenario.tags.filter(it => it.name === '@visual').length > 0) {
        //     await browser.vDriver.stopSession();
        // }
    },

    beforeFeature: function (uri, feature, scenarios) {

    },
    //
    // Hook that gets executed after the suite has ended
    // afterSuite: function afterSuite(suite) {
    // },
    //
    // Gets executed after all tests are done. You still have access to all
    // global variables from the test.
    // after: function after(result, capabilities, specs) {
    // },
    //
    // Gets executed after all workers got shut down and the process is about to
    // exit. It is not possible to defer the end of the process using a promise.
    onComplete: function() {
        filesUtil.rmdir(downloadDir);
      }
};
