const path = require('path');
const { config } = require('./wdio.conf.js');
global.downloadDir = path.join(__dirname, 'tempDownload');

config.user = 'siarheimalaichuk1';  //|| 'BROWSERSTACK_USERNAME',
config.key = 'PzH2xPx3j4wd3bCqQRjy'; //|| 'BROWSERSTACK_ACC_KEY',
config.maxInstances = 3;

config.capabilities = [
    {
        'bstack:options': {
            'os': 'OS X',
            'osVersion': 'Mojave',
            'projectName': 'Test',
            'buildName': 'OSX-Chrome-81',
            'local': 'false',
            'consoleLogs': 'warnings',
            'networkLogs': 'true',
        },
        'browserName': 'Chrome',
        'browserVersion': '81.0',
        'goog:chromeOptions': {
            args: [
                '--disable-infobars',
                '--no-sandbox',
                //'--headless',
                '--disable-gpu',
                '--disable-setuid-sandbox',
                '--disable-dev-shm-usage',
            ],
            prefs: {
                'directory_upgrade': true,
                'download.prompt_for_download': false,
                'plugins.always_open_pdf_externally': true,
                'download.default_directory': downloadDir
            },
        },
    },
    // {
    //     'bstack:options': {
    //         'os': 'Windows',
    //         'osVersion': '10',
    //         'projectName': 'Test',
    //         'buildName': 'WIN10-Chrome-81',
    //         'local': 'false',
    //         'networkLogs': 'true',
    //     },
    //     'browserName': 'Chrome',
    //     'browserVersion': '81.0',
    //     'goog:chromeOptions': {
    //         args: [
    //             '--disable-infobars',
    //             '--no-sandbox',
    //             //'--headless',
    //             '--disable-gpu',
    //             '--disable-setuid-sandbox',
    //             '--disable-dev-shm-usage',
    //         ],
    //         prefs: {
    //             'directory_upgrade': true,
    //             'download.prompt_for_download': false,
    //             'plugins.always_open_pdf_externally': true,
    //             'download.default_directory': downloadDir
    //         },
    //     },
    // },
    // {
    //     'bstack:options': {
    //         'os': 'Windows',
    //         'osVersion': '10',
    //         'projectName': 'Test',
    //         'buildName': 'bs-1.0',
    //         'local': 'false',
    //         'consoleLogs': 'warnings',
    //         'networkLogs': 'true',
    //     },
    //     'browserName': 'Firefox',
    //     'browserVersion': '70.0',
    // },
    // {
    //     'bstack:options': {
    //         'os': 'OS X',
    //         'osVersion': 'Mojave',
    //         'projectName': 'Test',
    //         'buildName': 'bs-1.0',
    //         'local': 'false',
    //         'consoleLogs': 'warnings',
    //         'networkLogs': 'true',
    //     },
    //     'browserName': 'Safari',
    //     'browserVersion': '12.0',
    // },
];


config.beforeFeature = () => {
};

if (process.env.CI) {
    config.outputDir = path.join(__dirname, 'logs');
}
exports.config = config;
