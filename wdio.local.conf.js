/**
 * Only single local google chrome browser using chromedriver service
 * */

const path = require('path');
const { config } = require('./wdio.conf.js');

global.downloadDir = path.join(__dirname, 'tempDownload');

config.maxInstances = 1;
config.port = 9515;
config.path  = '/';
config.services.push(
    [
        'chromedriver',
        {
            chromeDriverArgs: ['--port=9516', '--url-base=\'/\''],
        },
    ],
);

config.capabilities = [
    {
        browserName: 'chrome',
        'goog:chromeOptions': {
            args: [
                '--disable-infobars',
                '--no-sandbox',
                //'--headless',
                '--disable-gpu',
                '--disable-setuid-sandbox',
                '--disable-dev-shm-usage',
            ],
            prefs: {
                'directory_upgrade': true,
                'prompt_for_download': false,
                'plugins.always_open_pdf_externally': true,
                'download.default_directory': downloadDir
            },
        },
    },
];

exports.config = config;
