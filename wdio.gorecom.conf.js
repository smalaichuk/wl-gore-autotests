const PROVIDER = process.env.PROVIDER;
const { config } = require(`./wdio.${PROVIDER}.conf.js`);

config.cucumberOpts.require.push('./src/steps/goreCom/*.js');
config.baseUrl = 'https://www.gore.com/';

exports.config = config;
