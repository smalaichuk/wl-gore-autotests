import allureReporter from '@wdio/allure-reporter';
import { BeforeAll } from 'cucumber';
const { Before, After, Status } = require('cucumber');
const argv = require("yargs").argv;

const fs = require('fs');

function decode(png) {
    const decodedImage = new Buffer(png, 'base64');
    return decodedImage;
}

function attachImage(path, name) {
    allureReporter.addAttachment(name, decode(fs.readFileSync(path)), 'image/png');
}

BeforeAll(function (scenario) {
    // extend browser and element objects
    function we(locator, timeout = 5000) {
        try {
            const el = this.$(locator);
            // el.moveTo();
            el.scrollIntoView();
            el.waitForEnabled(timeout);
            return el;
        } catch (err) {
            throw `Cannot wait for element enabled with locator: '${locator}',
                            displayed within: '${timeout}' seconds, error: '${err}'`;
        }
    }

    browser.addCommand('we', we, true);
    browser.addCommand('we', we, false);

    function wd (locator, timeout = 5000) {
        try {
            const el = this.$(locator);
            el.waitForDisplayed(timeout);
            return el;
        } catch (err) {
            throw `Cannot wait for element displayed with locator: '${locator}',
                            displayed within: '${timeout}' seconds, error: '${err}'`;
        }
    }

    browser.addCommand('wd', wd, true);
    browser.addCommand('wd', wd, false);

});


Before(function (scenario, callback) {
    this.STATE.currentScenario = scenario;
    browser.maximizeWindow();
    callback();
});

After(function (scenarioResult) {
    allureReporter.addArgument("Platform", browser.capabilities.platform);
    allureReporter.addArgument("Environment", process.env.APP);
    if (scenarioResult.result.status === 'failed') {

        // Save screenshot and debuginfo to local folder
        const fName = './tests/debugInfo/'
            + new Date().getTime()
            + scenarioResult.pickle.name.split('')
                .join('');
        browser.saveScreenshot(fName + '.png');
        fs.writeFileSync(fName + '.txt', JSON.stringify(browser.options, null, 2));
        console.log(`debug info was saved to ${fName}.* files`);

        // Attach screenshot to reporting
        browser.takeScreenshot();
        allureReporter.addAttachment('testDetails.txt', JSON.stringify(scenarioResult));

        const ss = browser.takeScreenshot();
        const coockies = JSON.stringify(browser.getAllCookies());

        allureReporter.addAttachment('screenshot', decode(ss), 'image/png');
        allureReporter.addAttachment('source.html', browser.$('html')
            .getHTML(), 'text/html');
        allureReporter.addAttachment('cookies.txt', coockies);
        console.log(browser.sharedStore.diff);
        if(browser.sharedStore.get('diff') !== '' && browser.sharedStore.get('diff') !== undefined)
        {
            allureReporter.addLabel("testType", "screenshotDiff");
            attachImage(browser.sharedStore.get('actual'), 'actual');
            attachImage(browser.sharedStore.get('expected'), 'expected');
            attachImage(browser.sharedStore.get('diff'), 'diff');
        }
    }
    if(process.env.PROVIDER === "bs")
    {
        allureReporter.addAttachment("BrowserStack Session", browser.sessionId);
    }
    if (argv.parallel !== 'true') {
        browser.reloadSession();
    }
    return scenarioResult.status;
});
