/**
 * Check if the given element exists in the current DOM
 * @param  {String}   selector  Element selector
 * @param  {String}   falseCase Whether to check if the element exists or not
 */
export default (selector, falseCase) => {
    /**
     * Elements found in the DOM
     * @type {Object}
     */

    if (falseCase) {
        const elements = $$(selector);
        expect(elements).to.have
            .lengthOf(0, `Expected element "${selector}" not to exist`);
    } else {
        $(selector).waitForExist(2000);
        const elements = $$(selector);
        expect(elements).to.have.length
            .above(0, `Expected element "${selector}" to exist`);
    }
};
