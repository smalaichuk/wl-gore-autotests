const testData = require("../testData");
/**
 * Check if the given element is (not) visible
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Check for a visible or a hidden element
 */
export default (selector, falseCase) => {
    /**
     * Visible state of the give element
     * @type {String}
     */
    browser.pause(1700);
    try{
        const re = /(contains(.*)(("|')([A-Z]{2}.@([^'|"]+))("|')))/;
        const matchesArray = re.exec(selector); 
        if(testData.IsTesDataHasKey(matchesArray[5])){
            selector = selector.replace(matchesArray[5], testData.getValueByKey(matchesArray[5]));
        }    
        }
    catch(error){
    }
    const isDisplayed = $(selector).isDisplayed();
    // const isDisplayed = $(selector).waitForDisplayed();

    if (falseCase) {
        expect(isDisplayed).to.not
            .equal(true, `Expected element "${selector}" not to be displayed`);
    } else {
        expect(isDisplayed).to
            .equal(true, `Expected element "${selector}" to be displayed`);
    }
};
