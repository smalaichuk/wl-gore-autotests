const pdf = require('pdf-parse');
const fs = require('fs');
const path = require('path');
const { URL } = require('url');
const { waitForFileExists } = require('../waiters');
const testData = require("../testData");

/**
 * Check if the file is donwloaded and include text
 * @param  {String}   selector  selector of download link with 'href' attibute
 * @param  {String}   text text that include file
*/
export default (selector, text) => {
  try{
    const re = /(contains(.*)(("|')([A-Z]{2}.@([^'|"]+))("|')))/;
    const matchesArray = re.exec(selector); 

    if(testData.IsTesDataHasKey(matchesArray[5])){
        selector = selector.replace(matchesArray[5], testData.getValueByKey(matchesArray[5]));
    }    
  }
  catch(error){
  }
  const downloadLink = $(selector);

  // get the value of the 'href' attibute on the download link
  const downloadHref = downloadLink.getAttribute('href');

  const downloadUrl = new URL(downloadHref);

  const fullPath = downloadUrl.pathname;

  const splitPath = fullPath.split('/');

  let fileName = splitPath.splice(-1)[0];

  if(testData.IsTesDataHasKey(text)){
    text = text.replace(text, testData.getValueByKey(text));
  } 

  const filePath = path.join(global.downloadDir, fileName);

  if(process.env.PROVIDER === 'bs'){
    browser.call(function() {
      browser.execute(`browserstack_executor: {"action": "fileExists", "arguments": {"fileName": "${fileName}"}}`).then(function(file_exists){
        console.log(file_exists);
      });
    });


    browser.call(function() {
      // download file
      browser.execute(`browserstack_executor: {"action": "getFileContent", "arguments": {"fileName": "${fileName}"}}`).then((get_file_content) => {
        fs.writeFile(`${fileName}`, get_file_content, {encoding: 'base64'}, function(err) {
          // decode the content to Base64
          console.log('File created');
        });
      });
    });

    browser.call(function (){
      // call function that checks for the file to exist
      return waitForFileExists(fileName, 60000)
    });
  }
  else {
    browser.call(function (){
      // call function that checks for the file to exist
      return waitForFileExists(filePath, 60000)
    });
    fileName = filePath;
  }
  let dataBuffer = fs.readFileSync(`${fileName}`);

  browser.call(async function(){
    const data = await pdf(dataBuffer);
    expect(data.text).to.include(text);
  });
  if(process.env.PROVIDER === 'bs'){
    fs.unlinkSync(`${fileName}`);
  }
}