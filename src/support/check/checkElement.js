/**
 * Check if element is equal to baselineImage
 * @param  {String}  baslineImage  name of baseline Image to comparing 
 */
export default (locator, baslineImage) => {
    const elementSreen = browser.checkElement($(locator), baslineImage, { returnAllCompareData: true });    
    if(elementSreen.misMatchPercentage > 2)
    { 
        browser.sharedStore.set('diff', elementSreen.folders.diff);
        browser.sharedStore.set('actual', elementSreen.folders.actual);
        browser.sharedStore.set('expected', elementSreen.folders.baseline);
    }
    expect(elementSreen.misMatchPercentage).to.be.below(2);
};