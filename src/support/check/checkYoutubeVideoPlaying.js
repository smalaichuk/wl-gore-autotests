const fs = require('fs');
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const testData = require("../testData");
import { expect } from 'chai';

/**
 * Check if the youtube video is playing
 * @param  {String} id Id of a frame
 */
export default (id) => {
    try {
        $("//div[contains(@id,'slider-control-SI')]").waitForDisplayed(8000);
        $("//div[contains(@id,'slider-control-SI')]").click();
        $("//img[contains(@src,'close')]").waitForDisplayed(2000);
        browser.execute("arguments[0].click();", $("//img[contains(@src,'close')]"));
    }
    catch(Exception)
    {
        console.log("Feedback area is not present");
    }
    try{
        const re = /("|')yt_([A-Z]{2}.@([^'|"]+))("|')/;
        const matchesArray = re.exec(id); 
        if(testData.IsTesDataHasKey(matchesArray[2])){
            id = id.replace(matchesArray[2], testData.getValueByKey(matchesArray[2]));
        }    
    }
    catch(error){
    }

    try{
        const re = /("|')([A-Z]{2}.@([^'|"]+))("|')/;
        const matchesArray = re.exec(id); 
        if(testData.IsTesDataHasKey(matchesArray[2])){
            id = id.replace(matchesArray[2], testData.getValueByKey(matchesArray[2]));
        }    
    }
    catch(error){
    }
    $(id).waitForDisplayed(5000);
    browser.switchToFrame(browser.$(id));
    const screen1path = `./tests/reports/${Date.now()}.png`;
    if(id.includes('yt')){
        //for youtube videos
        try{
            $("//button[@class='ytp-play-button ytp-button']").waitForDisplayed(5000);
            $("//button[@class='ytp-play-button ytp-button']").click();
            $("//button[@class='ytp-fullscreen-button ytp-button']").click();

        }
        catch(e){
        }
    }
    else{
        //for chineese videos
        try{
            $("//div[@sub-component='play']/*/*[3]").waitForDisplayed(20000);
            $("//div[@sub-component='play']/*/*[3]").click();
            $("//button[@sub-component='fullscreen_btn']").click();
            $("//button[@now='pause']").waitForDisplayed(20000);
        }
        catch(e){
        }
    }
    browser.saveScreenshot(screen1path);
    browser.pause(7500);
    const screen2path = `./tests/reports/${Date.now()}.png`;
    browser.saveScreenshot(screen2path);

    const img1 = PNG.sync.read(fs.readFileSync(screen1path));
    const img2 = PNG.sync.read(fs.readFileSync(screen2path));
    const {width, height} = img1;
    const diff = new PNG({width, height});
    
    const difference = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.1});
    
    const disimilarity = difference * 100 / (width * height);
    expect(disimilarity).to.be.above(10,`Expected that images are not the same but disimilarity is ${disimilarity}%`);
    console.log(`${difference} pixels differents`);
}