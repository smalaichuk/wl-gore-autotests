/**
 * Check that count are (not) equal
 * @param  {Type}     falseCase  Check for a equal or not
 * @param  {Type}     selector selector of elements     
 * @param  {Type}     count2 second count 
 */
export default (selector, falseCase, count2) => {

    browser.pause(2000);
    const count1 = $$(selector).length.toString();
    
    if(count2 == "contextLength")
    {
        count2 = browser.sharedStore.get('contextLength').toString();
    }
    
    if (falseCase == " not") {
        expect(count1).to.not
            .equal(
                count2,
                `"${count1}" not equal to "${count2}"`
            );
    } else {
        expect(count1).to
            .equal(
                count2,
                `"${count1}" equal to "${count2}"`
            );
    }
};