const testData = require("../testData");
/**
 * Check if the given element is visible inside the current viewport
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Whether to check if the element is visible
 *                              within the current viewport or not
 */
export default (selector, falseCase) => {
    /**
     * The state of visibility of the given element inside the viewport
     * @type {Boolean}
     */
    browser.pause(1500);
    try {
        const re = /(contains(.*)(("|')([A-Z]{2}.@([^'|"]+))("|')))/;
        const matchesArray = re.exec(selector); 
        console.log(matchesArray[5]);
        if(testData.IsTesDataHasKey(matchesArray[5])) {
            selector = selector.replace(matchesArray[5], testData.getValueByKey(matchesArray[5]));
        }    
    }
    catch(error) {
            console.log(error);
    }
    const isDisplayed = $(selector).isDisplayedInViewport();

    if (falseCase) {
        expect(isDisplayed).to.not
            .equal(
                true,
                `Expected element "${selector}" to be outside the viewport`
            );
    } else {
        expect(isDisplayed).to
            .equal(
                true,
                `Expected element "${selector}" to be inside the viewport`
            );
    }
};
