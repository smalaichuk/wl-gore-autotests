/**
 * Check if full page screen is equal to baselineImage
 * @param  {String}  baslineImage  name of baseline Image to comparing 
 */
export default (baslineImage) => {
    const fullPageSreen = browser.checkFullPageScreen(baslineImage, { returnAllCompareData: true, fullPageScrollTimeout: 3000,  saveAboveTolerance: 2});
    if(fullPageSreen.misMatchPercentage > 2)
    { 
        browser.sharedStore.set('diff', fullPageSreen.folders.diff);
        browser.sharedStore.set('actual', fullPageSreen.folders.actual);
        browser.sharedStore.set('expected', fullPageSreen.folders.baseline);
    }
    expect(fullPageSreen.misMatchPercentage).to.be.below(2);
};