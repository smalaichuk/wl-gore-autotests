/**
 * Check if page screen is equal to baselineImage
 * @param  {String}  baslineImage  name of baseline Image to comparing 
 */
export default (baslineImage) => {
    const pageScreen = browser.checkScreen(baslineImage, { returnAllCompareData: true});
    if(pageScreen.misMatchPercentage > 2)
    { 
        browser.sharedStore.set('diff', pageScreen.folders.diff);
        browser.sharedStore.set('actual', pageScreen.folders.actual);
        browser.sharedStore.set('expected', pageScreen.folders.baseline);
    }
    expect(pageScreen.misMatchPercentage).to.be.below(2);
};