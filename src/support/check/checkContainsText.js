const testData = require("../testData");
/**
 * Check if the given elements contains text
 * @param  {String}   elementType   Element type (element or button)
 * @param  {String}   selector       Element selector
 * @param  {String}   falseCase     Whether to check if the content contains
 *                                  the given text or not
 * @param  {String}   expectedText  The text to check against
 */
export default (elementType, selector, falseCase, expectedText) => {
    /**
     * The command to perform on the browser object
     * @type {String}
     */
    let command = 'getValue';

    if (
        ['button', 'container'].includes(elementType)
        || $(selector).getAttribute('value') === null
    ) {
        command = 'getText';
    }

    /**
     * False case
     * @type {Boolean}
     */
    let boolFalseCase;

    /**
     * The expected text
     * @type {String}
     */
    
    let stringExpectedText = "";
    if(expectedText == "contextText")
    {
        stringExpectedText = browser.sharedStore.get('contextText');
    }
    else if(testData.IsTesDataHasKey(expectedText)){
        stringExpectedText = testData.getValueByKey(expectedText);
    }
    else{
        stringExpectedText = expectedText;
    }

    /**
     * The text of the element
     * @type {String}
     */
    const elem = $(selector);
    elem.scrollIntoView();
    browser.pause(500);
    elem.waitForDisplayed();
    const text = elem[command]();

    if (typeof expectedText === 'undefined') {
        stringExpectedText = falseCase;
        boolFalseCase = false;
    } else {
        boolFalseCase = (falseCase === ' not');
    }

    if (boolFalseCase) {
        expect(text).to.not.contain(stringExpectedText);
    } else {
        expect(text).to.contain(stringExpectedText);
    }
};
