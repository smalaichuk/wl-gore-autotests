/**
 * Get text and set it  to shared context 
 * @param  {String} attribute attribute to get text from
 * @param  {String} selector Element selector
 */
export default (attribute, selector) => {
    let text;
    if(attribute === 'text')
    {
        text = $(selector).getText();
    }
    if(attribute === 'href')
    {
        text = $(selector).getAttribute('href');
    }
    browser.sharedStore.set('contextText', text)
};