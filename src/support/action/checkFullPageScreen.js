/**
 * Check screenshoot of whole web page using page scrolling
 * @param  {String}   tag  a tag of the screenshoot can be used in image name
 * @param  {object}  opts   amethod options, see the reference on https://github.com/wswebcreation/webdriver-image-comparison/blob/master/docs/OPTIONS.md
 * */
export default (tag) => {
    return browser.checkFullPageScreen(tag, { returnAllCompareData: true });
};
