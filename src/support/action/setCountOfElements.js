/**
 * Get length and set it to shared context 
 * @param  {String} selector Element selector
 */
export default (selector) => {
    const length = $$(selector).length;
    browser.sharedStore.set('contextLength', length)
};