/**
 * Navigate backwards in the browser history
 */
export default () => {
    browser.back();
    browser.setTimeout({ 'pageLoad': 10000 });
};