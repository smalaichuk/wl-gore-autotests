const testData = require("../testData");
/**
 * Perform an click action on the given element
 * @param  {String}   action  The action to perform (click or doubleClick)
 * @param  {String}   type    Type of the element (link or selector)
 * @param  {String}   selector Element selector
 */
export default (action, type, selector) => {
    /**
     * Element to perform the action on
     * @type {String}
     */
    if(testData.IsTesDataHasKey(selector)){
        selector = testData.getValueByKey(selector);
    }
    const selector2 = (type === 'link') ? `=${selector}` : `${type}=${selector}`;

    /**
     * The method to call on the browser object
     * @type {String}
     */
    const method = (action === 'click') ? 'click' : 'doubleClick';
    
    try {
        $(selector2).scrollIntoView();
        $(selector2)[method]();
    }
    catch(Exception)
    {
        //perform js click
        browser.execute("arguments[0].click();", $(selector2));
    }
};
