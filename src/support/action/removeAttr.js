/**
 * Remove attribute from element
 * @param  {String}   selector  selector of the element
 * @param  {String}   attribute  attributee to remove from the element
 */
export default (attribute, selector) => {
    browser.execute("arguments[0].removeAttribute(arguments[1]);", $(selector), attribute);
};