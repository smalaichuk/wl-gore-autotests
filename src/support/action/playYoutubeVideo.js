/**
 * Play youtube video
 * @param  {String} selector id of youtube iframe
 */
export default (selector) => {
    const frame = browser.$(selector);
    browser.switchToFrame(frame);
    browser.$("button[aria-label='Play']")
        .click();
};