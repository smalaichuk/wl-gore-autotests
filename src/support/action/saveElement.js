/**
 * Save image of certain element
 * @param  {String}   id of an image
 * @param  {object}  opts   amethod options, see the reference on https://github.com/wswebcreation/webdriver-image-comparison/blob/master/docs/OPTIONS.md
 * */
export default (id) => {
    browser.saveElement($('#' + id));
};
