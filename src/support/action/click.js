const testData = require("../testData");
/**
 * Perform an click action on the given element
 * @param  {String}   selector Element selector
 */
export default (selector) => {
    try{
        const re = /(contains(.*)(("|')([A-Z]{2}.@([^'|"]+))("|')))/;
        const matchesArray = re.exec(selector); 

        if(testData.IsTesDataHasKey(matchesArray[5])){
            selector = selector.replace(matchesArray[5], testData.getValueByKey(matchesArray[5]));
        }    
    }
    catch(error){
    }
    $(selector).scrollIntoView();
    
    try {
        $(selector).click();
    }
    catch(Exception)
    {
        //perform js click
        browser.execute("arguments[0].click();", $(selector));
    }
};
