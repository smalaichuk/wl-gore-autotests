const testData = require("../testData");/**
 * Scroll the page to the given element
 * @param  {String}   selector Element selector
 */
export default (selector) => {
    try{const re = /(contains(.*)(("|')([A-Z]{2}.@([^'|"]+))("|')))/;
    const matchesArray = re.exec(selector); 

    if(testData.IsTesDataHasKey(matchesArray[5])){
        selector = selector.replace(matchesArray[5], testData.getValueByKey(matchesArray[5]));
    }}
    catch(error)
    {
    }
    $(selector).scrollIntoView();
};
