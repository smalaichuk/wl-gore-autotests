var data = require("../../testData.json");

function IsTesDataHasKey(key)
{   
    let datumKey = key;
    const lang = datumKey.split('.')[0];
    datumKey = datumKey.replace(lang+'.', '');
    if(data.hasOwnProperty(lang)){
        return true;
    }
    else 
        return false;
}

function getValueByKey(key)
{
    if(IsTesDataHasKey(key)){    
        const lang = key.split('.')[0];
        const datumKey = key.replace(lang+'.', '');
        return data[lang][0][datumKey];
    }
    else 
        return "";
}

module.exports = {
    IsTesDataHasKey: IsTesDataHasKey,
    getValueByKey: getValueByKey  
 }