const { i } = require('../../src/lib/localization');
const { setWorldConstructor } = require('cucumber');

function CustomWorld() {
    console.log(`Prepare custom world for: '${browser.options.capabilities.browserName}'`);
    this.STATE = { description: 'The Object to store shared data between steps...' };
    this.STATE.lang = process.env.LOCALE ? process.env.LOCALE : 'en';
}

setWorldConstructor(CustomWorld);

const { defineParameterType } = require('cucumber');

// const lang = process.env.LOCALE ? process.env.LOCALE : 'en';


defineParameterType({

regexp: /[^"]*/,
    transformer: function (s) {
        return Promise.resolve(i(s, this));
    },
    // transformer: (s) => Promise.resolve(s.toUpperCase()),
    name: 'param',
});
