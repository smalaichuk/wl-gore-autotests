class basePage {
    constructor() {
        this.title = '';
    }

    get path() {
        return '';
    }

    open() {
        browser.url(this.path);
        return this;
    }
}

module.exports = basePage;
