import basePage from '../basePage';
// import {extendElement, elementProps, getElementName} from '../utills/helpers'

class HomePage extends basePage {
    constructor() {
        super();
    }

    /** Elements * */

    /** Inputs * */

    /** Actions * */

    open() {
        super.open(browser.config.baseUrl);
        return this;
    }
}

export default new HomePage();
