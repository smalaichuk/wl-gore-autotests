import basePage from '../../basePage';

class NarrowFilterCmp extends basePage {

    constructor() {
        super();
    }

    get el() {
        return $('section.filter');
    }

    /** Actions ** */

    narrowBy(criteria) {
        const critList = criteria.split(',')
            .map(it => it.trim());
        critList.forEach(function (cr) {
            // browser.pause(500);
            const locator = `//label[contains(., "${cr}")]/../input|//a[contains(., "${cr}")]`;
            $(locator)
                .waitForDisplayed();
            browser.we(locator)
                .click();
        });
    }
}

export default new NarrowFilterCmp();
