import basePage from '../../basePage';

class HeroCmp extends basePage {

    constructor() {
        super();
    }

    get el() {
        return $('.hero-carousel__img');
        // return $('.hero-carousel-img__bg');
    }

    /** Elements ** */
    get slicks() {
        return this.el.$$('//ul[@class="slick-dots"]//button');
    }

    /** Actions ** */
    next() {
        this.el.we('button.hero-carousel-img__button--next')
            .click();
    }

    prev() {
        this.el.we('button.hero-carousel-img__button--prev')
            .click();
    }
}

export default new HeroCmp();
