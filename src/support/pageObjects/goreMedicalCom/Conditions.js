import basePage from '../basePage';
import * as PO from '../goreCom/allPO';

// import {extendElement, elementProps, getElementName} from '../utills/helpers'

class Conditions extends basePage {
    constructor() {
        super();
    }

    /** Elements * */
    /** Actions * */

    // open() {
    //     super.open(browser.config.baseUrl);
    //     return this;
    // }

    search(text) {
        // const contentBefore = $('#block-goremed-content').getText();
        const contentLengthBefore = $('#block-goremed-content')
            .getText().length;
        browser.we('//div[@class="spacing-zero"]/input[@id="site-search"]')
            .setValue(text);
        browser.we('//section[@class="filter__keyword"]//button')
            .click();
        browser.waitUntil(
            () => contentLengthBefore !== $('#block-goremed-content')
                .getText().length,
            5000,
            'Content length before search and after is the same!'
        );
    }

    /** Inputs * */
}

export default new Conditions();
