import basePage from './basePage'
import table from './table'


class AppPage extends basePage {

    constructor() {
        super();
        this.title = 'ACME demo app';
    }

    get path() {
        return appPagePath;
    }

    /** ELEMENTS **/

    get logo() {
        return $('.logo-element');
    }

    get expensesLink() {
        return $('#showExpensesChart');
    }

    get nextYearDataLink() {
        return $('.btn.btn-warning');
    }

    get firstBanner() {
        return $('#flashSale > img');
    }

    get secondBanner() {
        return $('#flashSale2 > img');
    }

    /**
     * Get amounts and convert them to floats, return array of floats
     * */
    get Amounts() {
        return this.Table.getColumn('AMOUNT').map((num) => {
            return parseFloat(
                num.getText()
                    .replace('USD', '')
                    .replace(',', '')
                    .replace(' ', ''))
        });
    }

    get Table() {
        return table;
    }

    addAd() {
        browser.url(this.path + '?showAd=true');
    }

    open() {
        super.open();
        this.expensesLink.waitForDisplayed(5000);
        return this
    }
}

export default new AppPage()
