class Table {

    get tableHead() {
        return $$('//th');
    }

    headItem(headName){
        return this.tableHead.filter(th => th.getText() === headName)[0];
    }

    getHeadNo(headName){
        return this.tableHead.findIndex(th => th.getText() === headName);
    }

    getColumn(columnName){
        let rows = $$('tr');
        rows.splice(0,1);
        return rows.map(row => (row.$$('td'))[this.getHeadNo(columnName)]);
    }

}

export default new Table()
