import basePage from '../../basePage';

class NavbarCmp extends basePage {

    constructor() {
        super();
    }

    get el() {
        return $('//header/div');
    }

    /** Actions ** */

    goto(menuItem) {
        this.el.$(`=${menuItem}`)
            .click();
    }

    search(keyword) {
        this.el.we('a.icon-i-search-small')
            .click();
        // this.el.we('#site-search');
        this.el.we('#site-search')
            .addValue(keyword);
        this.el.we('button.site-search__button')
            .click();
    }

    switchLanguage(lang) {
        this.el.we('#region-select')
            .click();
        this.el.we(`option=${lang}`)
            .click();
    }
}

export default new NavbarCmp();
