import basePage from '../../basePage';

class HeroCmp extends basePage {

    constructor() {
        super();
    }

    get el() {
        return $('section.homepage-hero');
    }

    /** Elements ** */
    get slicks() {
        return this.el.$$('//ul[@class="slick-dots"]//button');
    }

    /** Actions ** */
    next() {
        browser.pause(1000);
        // this.el.we('(//div[@id="slick-slide00"]//a[@tabindex="0"])[2]')
        this.el.we('div.next-slide.show-at-xlarge > a[tabindex="0"]')
            .click();
    }

    prev() {
        browser.pause(1000);
        this.el.we('a[data-tracklinktext=prev]')
            .click();
    }
}

export default new HeroCmp();
