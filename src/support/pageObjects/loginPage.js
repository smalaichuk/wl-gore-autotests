import basePage from './basePage'
import {extendElement, elementProps, getElementName} from '../utills/helpers'

class LoginPage extends basePage {

    constructor() {
        super();
        this.title = 'ACME demo app';
    }

    get path() {
        return loginPagePath;
    }

    /** ELEMENTS **/

    get logo() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.logo-w');
        return extendElement(element, rightLocation, rightSize);
    }

    get userIcon() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.pre-icon.os-icon.os-icon-user-male-circle');
        return extendElement(element, rightLocation, rightSize);
    }

    get passwordIcon() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.pre-icon.os-icon.os-icon-fingerprint');
        return extendElement(element, rightLocation, rightSize);
    }

    get loginFieldLabel() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('//form/div[1]/label');
        return extendElement(element, rightLocation, rightSize);
    }

    get passwordFieldLabel() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('//form/div[2]/label');
        return extendElement(element, rightLocation, rightSize);
    }

    get formLabel() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.auth-header');
        return extendElement(element, rightLocation, rightSize);
    }

    get loginForm() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.all-wrapper.menu-side.with-pattern');
        return extendElement(element, rightLocation, rightSize);
    }

    get checkboxLabel() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.form-check-label');
        return extendElement(element, rightLocation, rightSize);
    }

    /** inputs **/

    get passwordField() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('#password');
        return extendElement(element, rightLocation, rightSize);
    }

    get loginField() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('#username');
        return extendElement(element, rightLocation, rightSize);
    }

    get loginField() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('#username');
        return extendElement(element, rightLocation, rightSize);
    }

    get passwordField() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('#password');
        return extendElement(element, rightLocation, rightSize);
    }

    get alertArea() {
        const rightLocation = {x: 0, y: 0};
        const rightSize = {width: 0, height: 0};
        const element = $('.alert.alert-warning');
        return extendElement(element, rightLocation, rightSize);
    }

    get rememberCheckbox() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.form-check-input');
        return extendElement(element, rightLocation, rightSize);
    }


    get loginButton() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $('.btn.btn-primary');
        return extendElement(element, rightLocation, rightSize);
    }

    /** social networks icons **/

    get twitterImage() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $("(//div[@class='buttons-w']//a[1]/img)|(//div[@class='buttons-w']//span[1]/img)");
        return extendElement(element, rightLocation, rightSize);
    }

    get twitterLink() {
        return $("//div[@class='buttons-w']//a[1]")
    }

    get facebookImage() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $("(//div[@class='buttons-w']//a[2]/img)|(//div[@class='buttons-w']//span[2]/img)");
        return extendElement(element, rightLocation, rightSize);
    }

    get facebookLink() {
        return $("//div[@class='buttons-w']//a[2]")
    }

    get linkedinImage() {
        const elementName = getElementName();
        const rightLocation = elementProps(elementName).location;
        const rightSize = elementProps(elementName).size;
        const element = $("(//div[@class='buttons-w']//a[3]/img)|(//div[@class='buttons-w']//span[3]/img)");
        return extendElement(element, rightLocation, rightSize);
    }

    get linkedinLink() {
        return $("//div[@class='buttons-w']//a[3]")
    }

    /** ACTIONS **/

    login(username, password) {
        this.loginField.setValue(username);
        this.passwordField.setValue(password);
        this.loginButton.click();
        return this;
    }

    open() {
        super.open();
        this.loginButton.waitForDisplayed(5000);
        return this
    }
}

export default new LoginPage()
