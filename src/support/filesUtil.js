const fs = require('fs');
const { join } = require('path');

function rmdir(dir) {
    var list = fs.readdirSync(dir);
    for(var i = 0; i < list.length; i++) {
      var filename = join(dir, list[i]);
      var stat = fs.statSync(filename);
  
      if(filename == "." || filename == "..") {
        // pass these files
      } else if(stat.isDirectory()) {
        // rmdir recursively
        rmdir(filename);
      } else {
        // rm fiilename
        fs.unlinkSync(filename);
      }
    }
    fs.rmdirSync(dir);
}

module.exports = {
    rmdir: rmdir 
 }