function i(text, world) {
    const fs = require('fs');
    let specificLocales = {};
    let commonLocales = {};
    // console.log(world);
    if (world) {
        // console.log(world.STATE.currentScenario.sourceLocation.uri);
        const locPath = process.cwd() + '/' + world.STATE.currentScenario.sourceLocation.uri + '.locale.json';

        if (fs.existsSync(locPath)) {
            specificLocales = require(locPath);
        }
    }
    // const lang = locale ? locale : 'en';
    const lang = world.STATE.lang;

    // const lang = process.env.LOCALE ? process.env.LOCALE : 'en';
    const commonLocalesPath = `./localization.${lang}.json`;

    if (fs.existsSync(process.cwd() + '/src/lib/' + commonLocalesPath)) {
        commonLocales = require(commonLocalesPath);
    }
    const locales = { ...commonLocales, ...specificLocales[lang] };
    if (locales[text]) {
        return locales[text];
    }
    return text;
}

exports.i = i;
