import clearInputField from '../support/action/clearInputField';
import clickElement from '../support/action/clickElement';
import closeLastOpenedWindow from '../support/action/closeLastOpenedWindow';
import deleteCookies from '../support/action/deleteCookies';
import dragElement from '../support/action/dragElement';
import focusLastOpenedWindow from '../support/action/focusLastOpenedWindow';
import handleModal from '../support/action/handleModal';
import moveTo from '../support/action/moveTo';
import pause from '../support/action/pause';
import pressButton from '../support/action/pressButton';
import scroll from '../support/action/scroll';
import selectOption from '../support/action/selectOption';
import selectOptionByIndex from '../support/action/selectOptionByIndex';
import setCookie from '../support/action/setCookie';
import setInputField from '../support/action/setInputField';
import setPromptText from '../support/action/setPromptText';
import saveFullPageScreen from '../support/action/saveFullPageScreen';
import saveScreen from '../support/action/saveScreen';
import saveElement from '../support/action/saveElement';
import checkFullPageScreen from '../support/action/checkFullPageScreen';
import checkScreen from '../support/action/checkScreen';
import checkElement from '../support/action/checkElement';
import openWebsite from '../support/action/openWebsite';
import playYoutubeVideo from '../support/action/playYoutubeVideo'
import click from '../support/action/click';
import setTextFromElement from '../support/action/setTextFromElement';
import setCountOfElements from '../support/action/setCountOfElements';
import browserBack from '../support/action/browserBack';
import removeAttr from '../support/action/removeAttr';

const csvparse = require('csv-parse/lib/sync');
const got = require('got');

const { Given, When, Then } = require('cucumber');

When(
    /^I (click|doubleclick) on the (link|button|element) "([^"]*)?"$/,
    clickElement
);

When(/^I set (text|href) to sharedContext from '(.*)' element$/, 
    setTextFromElement
);

When(/^I set count of '(.*)' elements to sharedContext$/, 
    setCountOfElements
);

When(/^I navigate back in browser$/, 
    browserBack
);

When(
    /^I click on the "([^"]*)?"$/,
    click
);

When(
    /^I (add|set) "([^"]*)?" to the inputfield "([^"]*)?"$/,
    setInputField
);

When(
    /^I clear the inputfield "([^"]*)?"$/,
    clearInputField
);

When(
    /^I play youtube video "(.*)"$/,
    playYoutubeVideo
)

When(
    /^I drag element "([^"]*)?" to element "([^"]*)?"$/,
    dragElement
);

When(
    /^I pause for (\d+)ms$/,
    pause
);

When(
    /^I set a cookie "([^"]*)?" with the content "([^"]*)?"$/,
    setCookie
);

When(
    /^I delete the cookie "([^"]*)?"$/,
    deleteCookies
);

When(
    /^I press "([^"]*)?"$/,
    pressButton
);

When(
    /^I (accept|dismiss) the (alertbox|confirmbox|prompt)$/,
    handleModal
);

When(
    /^I enter "([^"]*)?" into the prompt$/,
    setPromptText
);

When(
    /^I scroll to element "([^"]*)?"$/,
    scroll
);

When(
    /^I close the last opened (window|tab)$/,
    closeLastOpenedWindow
);

When(
    /^I focus the last opened (window|tab)$/,
    focusLastOpenedWindow
);

When(
    /^I select the (\d+)(st|nd|rd|th) option for element "([^"]*)?"$/,
    selectOptionByIndex
);

When(
    /^I select the option with the (name|value|text) "([^"]*)?" for element "([^"]*)?"$/,
    selectOption
);

When(
    /^I move to element "([^"]*)?"(?: with an offset of (\d+),(\d+))*$/,
    moveTo
);

Given(/^I open "([^"]*)?"$/, (url) => {
        openWebsite('url', url);
    }
);

/**
 *      CUSTOM STEPS
 * */
When(/^I wait for "([^"]*)" seconds$/, { timeout: 7000000 }, (second) => {
    browser.pause(second * 1000);
});

When(/^I save a full page screenshot with tag "([^"]*)"$/,
    saveFullPageScreen);

When(/^I check a full page with tag "([^"]*)"$/,
    checkFullPageScreen);

When(/^I save a page screenshot with tag "([^"]*)"$/,
    saveScreen);

When(/^I check a page with tag "([^"]*)"$/,
    checkScreen);

When(/^I save an element image with id "([^"]*)"$/,
    saveElement);

When(/^I check an element image with tag "([^"]*)"$/,
    checkElement);

When(/^I remove "(.*)" attribute from "(.*)" element$/,
    removeAttr);

When(/^I debug "([^"]*)"$/, function (text) {
    // console.log(this.STATE.currentScenario);
});

Then(/^The screenshots have appeared in the Baseline folder$/, () => {
    // checkFullPageScreen
});

Then(/^All pages of the current site look correct$/, () => {
});

Given(/^the Baseline folder is clear$/, () => {
    const fse = require('fs-extra');
    fse.emptyDir('tests/BaselineImages');
});

Given(/^the Baseline folder contains images$/, () => {
    const testFolder = 'tests/BaselineImages';
    const fs = require('fs');

    function getFiles(dir, files_) {
        files_ = files_ || [];
        const files = fs.readdirSync(dir);
        for (const i in files) {
            const name = `${dir}/${files[i]}`;
            if (fs.statSync(name)
                .isDirectory()) {
                getFiles(name, files_);
            } else {
                files_.push(name);
            }
        }
        return files_;
    }

    if (getFiles(testFolder).length === 0) {
        const file = fs.readFileSync('DATA');
        throw 'Baseline folder is empty, please run collect baseline scenario';
    }
});

When(/^I check the page as "([^"]*)?"$/, { timeout: 700000 }, async function (checkName) {
    if (browser.vDriver === undefined) {
        throw new Error('Visual driver isn`t initialised, maybe you should set @visual tag for the scenario');
    }
    const vDriver = browser.vDriver;
    await vDriver.check({ name: checkName });
});

