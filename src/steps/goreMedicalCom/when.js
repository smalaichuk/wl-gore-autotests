import clearInputField from '../../support/action/clearInputField';
import clickElement from '../../support/action/clickElement';
import closeLastOpenedWindow from '../../support/action/closeLastOpenedWindow';
import deleteCookies from '../../support/action/deleteCookies';
import dragElement from '../../support/action/dragElement';
import focusLastOpenedWindow from '../../support/action/focusLastOpenedWindow';
import handleModal from '../../support/action/handleModal';
import moveTo from '../../support/action/moveTo';
import pause from '../../support/action/pause';
import pressButton from '../../support/action/pressButton';
import scroll from '../../support/action/scroll';
import selectOption from '../../support/action/selectOption';
import selectOptionByIndex from '../../support/action/selectOptionByIndex';
import setCookie from '../../support/action/setCookie';
import setInputField from '../../support/action/setInputField';
import setPromptText from '../../support/action/setPromptText';
import saveFullPageScreen from '../../support/action/saveFullPageScreen';
import saveScreen from '../../support/action/saveScreen';
import saveElement from '../../support/action/saveElement';
import checkFullPageScreen from '../../support/action/checkFullPageScreen';
import checkScreen from '../../support/action/checkScreen';
import checkElement from '../../support/action/checkElement';
import openWebsite from '../../support/action/openWebsite';
import allureReporter from '@wdio/allure-reporter';
import isExisting from '../../support/check/isExisting';
import * as PO from '../../support/pageObjects/goreMedicalCom/allPO';

// module.exports.HomePage = HomePage;

const csvparse = require('csv-parse/lib/sync');

const got = require('got');
const { Given, When, Then } = require('cucumber');

/** Common * */

// Make sure that certain PO has method open()
Given(/^I open "([^"]*)?" page$/, function (pageName) {
    PO[pageName].open();
});

// Any actions on the page
Given(/^I (search) for "([^"]*)?" on "([^"]*)?" page$/, function (action, params, pageName) {
    PO[pageName][action](params);
});

/** Navbar * */

When(/^I go to "([^"]*)" site section$/, function (menuName) {
    PO.NavBarCmp.goto(menuName);
});

When(/^I search for "([^"]*)"$/, function (keyword) {
    PO.NavBarCmp.search(keyword);
    browser.pause(1000);
});

When(/^I switch location to "([^"]*)"$/, function (lang) {
    const locale = {
        'Germany / Deutschland': 'de',
        'USA / United States': 'en',
    };
    this.STATE.lang = locale[lang];
    PO.NavBarCmp.switchLanguage(lang);
});

/** Hero * */

When(/^I switch click on "([^"]*)" slick on hero bar$/, function (slickNumber) {
    parseInt(slickNumber, 10);
    PO.HeroCmp.slicks[slickNumber - 1].click();
});

When(/^I follow (next|prev) link on hero bar$/, function (button) {
    PO.HeroCmp[button]();
});

/** Landing header */

When(/^I narrow page content by "([^"]*)"$/, function (criteria) {
    PO.NarrowFilter.narrowBy(criteria);
});


