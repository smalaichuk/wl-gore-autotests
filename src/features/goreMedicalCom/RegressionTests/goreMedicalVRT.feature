Feature: goreMedicalVRT 
    Check goremedical.com main page for locations

    Background: Open GoreCom
        Given I open the url "US.goreMedicalComLink"
        Then I expect that the url is "US.goreMedicalComLinkUrl"

@tc1 @visual
Scenario: Check main page for NA location
        Then I expect that the full page screenshot 'goremedicalMainPageNA' is equal to baseline

@tc2 @visual
Scenario Outline: Check main page for <location> location
        When I click on the "//select[@id='region-select']/option[contains(.,'<location>')]"
        When I remove "class" attribute from "//div[@role='listbox']" element
        Then I expect that the url is "<url>"
        And I expect that the full page screenshot '<baseline>' is equal to baseline 

    Examples:
        | location                           | url                               | baseline              |
        | US.@vrt.goreMAsiaTextLink          | US.@vrt.goreMAsiaLinkUrl          | goremedicalMainPageAP |
        | US.@vrt.goreMEuropeTextLink        | US.@vrt.goreMEuropeLinkUrl        | goremedicalMainPageEU |
        | US.@vrt.goreMLatinaAmericaTextLink | US.@vrt.goreMLatinaAmericaLinkUrl | goremedicalMainPageLA |
        | US.@vrt.goreMMEAfricaTextLink      | US.@vrt.goreMMEAfricaLinkUrl      | goremedicalMainPageME |

