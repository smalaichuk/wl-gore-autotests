Feature: goreComCH
    Check gore.com main page finctionality with CH location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'中国')]"
        Then I expect that the url is "gore.com.cn"

@tc1 @tc2
Scenario Outline: Validate that <link> link opens correctly
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
         
     Examples:
        | link                      | url                            | inputText                      |
        | CH.@tc1.ContactLink       | US.@tc1.ContactLinkUrl         | CH.@tc1.ContactInputText       |
        | CH.@tc1.ProductsLink      | US.@tc1.ProductsLinkUrl        | CH.@tc1.ProductsInputText      |
        | CH.@tc1.ResourcesLink     | US.@tc1.ResourceLibraryLinkUrl | CH.@tc1.ResourcesInputText     |
        | CH.@tc1.NewsLink          | US.@tc1.NewsEventsLinkUrl      | CH.@tc1.NewsInputText          |
        | CH.@tc1.AboutGoreLink     | US.@tc1.AboutGoreLinkUrl       | CH.@tc1.AboutGoreInputText     |
        | CH.@tc1.CareersLink       | US.@tc1.CareersLinkUrl         | CH.@tc1.CareersInputText       |
        | CH.@tc1.PrivacyNoticeLink | US.@tc1.PrivacyNoticeLinkUrl   | CH.@tc1.PrivacyNoticeInputText |
        | CH.@tc1.TermsOfUseLink    | CH.@tc1.TermsOfUseUrl          | CH.@tc1.TermsOfUseInputText    |
         | CH.@tc1.GovLink           | CH.@tc1.GovLinkUrl             | CH.@tc1.GovInputText           |
        

@tc2.1
Scenario: Validate that WeChat link opens popup window
        When I click on the link "CH.@tc2.1.WeChatLink"
        Then I expect that element "//div[@class='share-qr__content']/p" contains the text "CH.@tc2.1.WeChatInputText"

@tc3
Scenario: Validate Carousel on Homepage
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "CH.@tc3.carouselTitle1"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "CH.@tc3.carouselTitle2"
        When I follow prev link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "CH.@tc3.carouselTitle1"
       
@tc3.1
Scenario: Validate that Carousel Learn More links opens correctly
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'CH.@tc3.1.LearnMoreLink')]"
        Then I expect that the url is "CH.@tc3.1.CarouselLink2"
        And I expect that element "body" contains the text "US.@tc3.1.carouselText1"
        When I navigate back in browser
        When I scroll to element "//div[contains(@class,'slick-active')]//h2[contains(.,'CH.@tc3.carouselTitle1')]"
        When I follow next link on hero bar
        When I click on the link "CH.@tc3.1.LearnMoreLink"
        Then I expect that the url is "CH.@tc3.1.CaroselLink1"
        And I expect that element "body" contains the text "CH.@tc3.1.CarouselInputText1"

@tc4
Scenario Outline: Validate that gore main page appears <videoId> video
        When I click on the "//a[contains(@href,'<videoId>')]"
        Then I expect that video with id "//iframe[contains(@src,'<videoId>')]" is playing
    
    Examples: 
        | videoId          |
        | CH.@tc4.VideoId1 |
        | CH.@tc4.VideoId2 |
      
@tc5
Scenario Outline: Validate that <link> product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
        When I click on the link "CH.@tc1.ProductsLink"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        When I click on the "//h3/a[contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"

      Examples:
        | link                             | url                                 | inputText                             |
        | CH.@tc5.VentingLink              | US.@tc5.VentingLinkUrl              | CH.@tc5.VentingInputText              |
        | CH.@tc5.ElectronicComponentsLink | US.@tc5.ElectronicComponentsLinkUrl | CH.@tc5.ElectronicComponentsInputText |
        | CH.@tc5.FiltrationLink           | US.@tc5.FiltrationLinkUrl           | CH.@tc5.FiltrationInputText           |
        | CH.@tc5.SealantsLink             | US.@tc5.SealantsLinkUrl             | CH.@tc5.SealantsInputText             |
        | CH.@tc5.ConsumerProductsLink     | US.@tc5.ConsumerProductsLinkUrl     | CH.@tc5.ConsumerProductsInputText     |
        | CH.@tc5.FabricsLink              | US.@tc5.FabricsLinkUrl              | CH.@tc5.FabricsInputText              |
        | CH.@tc5.FibersLink               | US.@tc5.FibersLinkUrl               | CH.@tc5.FibersInputText               |
        | CH.@tc5.MedicalLink              | US.@tc5.MedicalLinkUrl              | CH.@tc5.MedicalInputText              |
        | CH.@tc5.PharmaceuticalLink       | US.@tc5.PharmaceuticalLinkUrl       | CH.@tc5.PharmaceuticalInputText       |

@tc5.1
Scenario: Validate that product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'CH.@tc5.1.CablesAssembliesLink')]"
        Then I expect that the url is "US.@tc5.CablesLinkUrl"
        And I expect that element "body" contains the text "CH.@tc5.1.CablesAssembliesInputText"
        When I click on the link "CH.@tc1.ProductsLink"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        When I click on the "//h3/a[contains(text(),'CH.@tc5.1.CablesAssembliesLink1')]"
        Then I expect that the url is "US.@tc5.CablesLinkUrl"
        And I expect that element "body" contains the text "CH.@tc5.1.CablesAssembliesInputText"

@tc6 @tc7
Scenario: Validate that main page appears texts and links
        When I click on the link "CH.@tc6.AboutGoreLink"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        And the title is "CH.@tc6.AboutGoreTitle"
        When I navigate back in browser
        And I click on the link "CH.@tc6.MoreTechnologiesLink"
        Then I expect that the url is "US.@tc6.MoreTechnologiesUrl"
        And the title is "CH.@tc6.MoreTechnologiesTitle"
        When I navigate back in browser
        And I click on the link "CH.@tc6.ExploreTechnologiesLink"
        Then I expect that the url is "US.@tc6.ExploreTechnologiesUrl"
        And the title is "CH.@tc6.ExploreTechnologiesTitle"
        When I navigate back in browser
        Then I expect that element "body" contains the text "CH.@tc1.NewsLink"
        And I expect that element "body" contains the text "CH.@tc6.InputText1"
        And I expect that element "body" contains the text "CH.@tc6.InputText2"
        And I expect that element "body" contains the text "CH.@tc6.InputText3"

@tc8 @tc11
Scenario Outline: Validate that the View Gore Products By <industry> works correctly
        When I click on the "//a[contains(text(),'CH.@tc8.ProductIndustriesLink')]"
        And I scroll to element "//div[@class='by-industry__list']//a[contains(text(),'<industry>')]"
        And I click on the link "<industry>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        When I click on the link "CH.@tc1.ProductsLink"
        And I click on the link "CH.@tc8.IndustriesLink"
        And I click on the "//h3/a[contains(text(),'<industry>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        
    Examples:
      | industry                      | url                          |
      | CH.@tc8.AerospaceLink         | US.@tc8.AerospaceUrl         |
      | CH.@tc8.ApparelTextilesLink   | US.@tc8.ApparelTextilesUrl   |
      | CH.@tc8.AutomotiveLink        | US.@tc8.AutomotiveUrl        |
      | CH.@tc8.ChemicalsLink         | US.@tc8.ChemicalsUrl         |
      | CH.@tc8.EnvProtectionLink     | US.@tc8.EnvProtectionUrl     |
      | CH.@tc8.FireSafetyLink        | US.@tc8.FireSafetyUrl        |
      | CH.@tc8.LifeSciencesLink      | US.@tc8.LifeSciencesUrl      |
      | CH.@tc8.MilitaryLink          | US.@tc8.MilitaryUrl          |
      | CH.@tc8.MobileElectronicsLink | US.@tc8.MobileElectronicsUrl |
      | CH.@tc8.OilGasLink            | US.@tc8.OilGasUrl            |
      | CH.@tc8.PowerUtilitiesLink    | US.@tc8.PowerUtilitiesUrl    |
      | CH.@tc8.SemiconductorLink     | US.@tc8.SemiconductorUrl     |
      | CH.@tc8.TechnologyLink        | US.@tc8.TechnologyUrl        |
      | CH.@tc8.TestMeasurementLink   | US.@tc8.TestMeasurementUrl   |

@tc9
Scenario: Validate that Product page appears texts and links
        When I click on the link "CH.@tc1.ProductsLink"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        And the title is "CH.@tc9.ProductsTitle"
        And I expect that element "body" contains the text "CH.@tc1.ProductsInputText"
        And I expect that element "body" contains the text "CH.@tc9.ProductsInputText1"
        And I expect that element "body" contains the text "CH.@tc1.ProductsLink"
        And I expect that element "body" contains the text "CH.@tc9.ProductsInputText2"
    
@tc10
Scenario: Validate that View all products page appear product
        When I click on the link "CH.@tc1.ProductsLink"
        And I click on the link "CH.@tc10.ViewAllProductsLink"
        Then I expect that the url is "US.@tc10.ViewAllProductsUrl"
        And I expect that element "//h2" contains the text "CH.@tc1.ProductsLink"
        And I set text to sharedContext from '//div[@class="column products-large-list"][2]//a' element
        And I click on the "//div[@class='column products-large-list'][2]//a"
        Then I expect that element "body" contains the text "contextText"