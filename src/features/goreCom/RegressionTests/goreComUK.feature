Feature: goreComUK 
    Check gore.com main page finctionality with UK location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'United Kingdom')]"
        Then I expect that the url is "gore.co.uk"

@tc1 @tc2
Scenario Outline: Validate that <link> link opens correctly
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
         
     Examples:
        | link                               | url                               | inputText                           |
        | US.@tc1.ContactLinkText            | US.@tc1.ContactLinkUrl            | US.@tc1.ContactInputText            |
        | US.@tc1.ProductsLinkText           | UK.@tc1.ProductsLinkUrl           | US.@tc1.ProductsInputText           |
        | US.@tc1.ResourceLibraryLinkText    | US.@tc1.ResourceLibraryLinkUrl    | US.@tc1.ResourceLibraryInputText    |
        | US.@tc1.NewsEventsLinkText         | US.@tc1.NewsEventsLinkUrl         | US.@tc1.NewsEventsInputText         |
        | US.@tc1.AboutGoreLinkText          | US.@tc1.AboutGoreLinkUrl          | US.@tc1.AboutGoreInputText          |
        | US.@tc1.CareersLinkText            | US.@tc1.CareersLinkUrl            | US.@tc1.CareersInputText            |
        | US.@tc1.PrivacyNoticeLinkText      | US.@tc1.PrivacyNoticeLinkUrl      | US.@tc1.PrivacyNoticeInputText      |
        | US.@tc1.TermsOfUseLinkText         | US.@tc1.TermsOfUseLinkUrl         | US.@tc1.TermsOfUseInputText         |   
        | UK.@tc1.CookieNoticeLinkText       | UK.@tc1.CookieNoticeLinkUrl       | UK.@tc1.CookieNoticeInputText       |
        | UK.@tc1.ModernSlaveryActLinkText   | UK.@tc1.ModernSlaveryActLinkUrl   | UK.@tc1.ModernSlaveryActInputText   |
        | UK.@tc1.GlobalTaxStrategyLinkText  | UK.@tc1.GlobalTaxStrategyLinkUrl  | UK.@tc1.GlobalTaxStrategyInputText  |
        | UK.@tc1.GenderPayGapReportLinkText | UK.@tc1.GenderPayGapReportLinkUrl | UK.@tc1.GenderPayGapReportInputText |
        | UK.@tc1.ImprintLinkText            | UK.@tc1.ImprintLinkUrl            | UK.@tc1.ImprintInputText            |
    

@tc2.1
Scenario: Validate that Youtube link opens correctly
        When I click on the link "YouTube"
        Then I expect the url "youtube.com/user/wlgoreassociates" is opened in a new tab

@tc3
Scenario: Validate Carousel on Homepage
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle1"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "UK.@tc3.carouselTitle1"
        When I follow prev link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle1"
       
@tc3.1
Scenario: Validate that Carousel Learn More links opens correctly
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'UK.@tc3.1.LearnMoreLink')]"
        Then I expect that the url is "CH.@tc3.1.CarouselLink2"
        And I expect that element "body" contains the text "US.@tc3.1.carouselText1"
        When I navigate back in browser
        When I follow next link on hero bar
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'UK.@tc3.1.LearnMoreLink')]"
        Then I expect that the url is "CH.@tc3.1.CaroselLink1"
        And I expect that element "body" contains the text "UK.@tc3.carouselTitle1"

@tc4
Scenario Outline: Validate that gore main page appears <youtubeId> youtube videos
        When I click on the "//a[contains(@href,'<youtubeId>')]"
        Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
    
    Examples: 
        | youtubeId          |
        | US.@tc4.YoutubeId1 |
        | US.@tc4.YoutubeId2 |
      
@tc5
Scenario Outline: Validate that <link> product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
        When I click on the link "US.@tc1.ProductsLinkText"
        Then I expect that the url is "UK.@tc1.ProductsLinkUrl"
        When I click on the "//h3/a[contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"

    Examples:
        | link                                 | url                                 | inputText                             |
        | US.@tc5.ConsumerProductsLinkText     | UK.@tc5.ConsumerProductsUrl         | US.@tc5.ConsumerProductsInputText     |
        | US.@tc5.CablesLinkText               | US.@tc5.CablesLinkUrl               | US.@tc5.CablesInputText               |
        | US.@tc5.ElectronicComponentsLinkText | US.@tc5.ElectronicComponentsLinkUrl | US.@tc5.ElectronicComponentsInputText |
        | US.@tc5.FabricsLinkText              | UK.@tc5.FabricsUrl                  | US.@tc5.FabricsInputText              |
        | US.@tc5.FibersLinkText               | UK.@tc5.FibresUrl                   | US.@tc5.FibersInputText               |
        | US.@tc5.FiltrationLinkText           | US.@tc5.FiltrationLinkUrl           | US.@tc5.FiltrationInputText           |
        | US.@tc5.MedicalLinkText              | US.@tc5.MedicalLinkUrl              | US.@tc5.MedicalInputText              |
        | US.@tc5.PharmaceuticalLinkText       | US.@tc5.PharmaceuticalLinkUrl       | US.@tc5.PharmaceuticalInputText       |
        | US.@tc5.SealantsLinkText             | US.@tc5.SealantsLinkUrl             | US.@tc5.SealantsInputText             |
        | US.@tc5.VentingLinkText              | US.@tc5.VentingLinkUrl              | US.@tc5.VentingInputText              |

@tc6 @tc7
Scenario: Validate that main page appears texts and links
        When I click on the link "US.@tc6.MoreAboutGore"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        And the title is "US.@tc6.AboutGoreTitle"
        When I navigate back in browser
        And I click on the link "US.@tc6.MoreTechnologies"
        Then I expect that the url is "US.@tc6.MoreTechnologiesUrl"
        And the title is "US.@tc6.MoreTechnologiesTitle"
        When I navigate back in browser
        And I click on the link "US.@tc6.ExploreTechnologies"
        Then I expect that the url is "UK.@tc6.ExploreTechnologiesUrl"
        And the title is "US.@tc6.ExploreTechnologiesTitle"
        When I navigate back in browser
        Then I expect that element "body" contains the text "US.@tc6.InputText1"
        And I expect that element "body" contains the text "US.@tc6.InputText2"
        And I expect that element "body" contains the text "UK.@tc6.InputText1"
        And I expect that element "body" contains the text "US.@tc6.InputText4"

@tc8 @tc11
Scenario Outline: Validate that the View Gore Products By <industry> works correctly
        When I click on the "//a[contains(text(),'US.@tc8.ViewGoreProducts')]"
        And I scroll to element "//div[@class='by-industry__list']//a[contains(text(),'<industry>')]"
        And I click on the link "<industry>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the link "US.@tc15.ResourceResultsInputText2"
        And I click on the "//h3/a[contains(text(),'<industry>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
          
    Examples:
        | industry                          | url                          |
        | US.@tc8.AerospaceIndustry         | UK.@tc8.AerospaceUrl         |
        | US.@tc8.ApparelTextilesIndustry   | UK.@tc8.ApparelTextilesUrl   |
        | US.@tc8.AutomotiveIndustry        | UK.@tc8.AutomotiveUrl        |
        | US.@tc8.ChemicalsIndustry         | UK.@tc8.ChemicalsUrl         |
        | US.@tc8.EnvProtectionIndustry     | UK.@tc8.EnvProtectionUrl     |
        | US.@tc8.FireSafetyIndustry        | UK.@tc8.FireSafetyUrl        |
        | US.@tc8.IndustrialIndustry        | UK.@tc8.IndustrialUrl        |
        | US.@tc8.LifeSciencesIndustry      | UK.@tc8.LifeSciencesUrl      |
        | US.@tc8.MilitaryIndustry          | UK.@tc8.MilitaryUrl          |
        | US.@tc8.MobileElectronicsIndustry | UK.@tc8.MobileElectronicsUrl |
        | US.@tc8.OilGasIndustry            | UK.@tc8.OilGasUrl            |
        | US.@tc8.PowerUtilitiesIndustry    | UK.@tc8.PowerUtilitiesUrl    |
        | US.@tc8.SemiconductorIndustry     | UK.@tc8.SemiconductorUrl     |
        | US.@tc8.TechnologyIndustry        | UK.@tc8.TechnologyUrl        |
        | US.@tc8.TestMeasurementIndustry   | UK.@tc8.TestMeasurementUrl   |

@tc9
Scenario: Validate that Product page appears texts and links
        When I click on the link "US.@tc1.ProductsLinkText"
        Then I expect that the url is "UK.@tc1.ProductsLinkUrl"
        And the title is "US.@tc9.ProductsTitle"
        And I expect that element "body" contains the text "US.@tc9.ProductsInputText1"
        And I expect that element "body" contains the text "US.@tc9.ProductsInputText2"
        And I expect that element "body" contains the text "US.@tc1.ProductsLinkText"
        And I expect that element "body" contains the text "US.@tc9.ProductsInputText3"

@tc10
Scenario: Validate that View all products page appear product
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the link "US.@tc10.ViewAllProductsLink"
        Then I expect that the url is "UK.@tc10.ProductUrl"
        And I expect that element "//h2" contains the text "US.@tc1.ProductsLinkText"
        When I set href to sharedContext from '//div[@class="column products-large-list"][1]//a' element
        And I click on the "//div[@class='column products-large-list'][1]//a"
        Then I expect the url "contextText" is opened in a new tab