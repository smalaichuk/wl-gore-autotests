Feature: goreComES 
    Check gore.com main page functionality with ES location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'España')]"
        Then I expect that the url is "gore.com.es"

@tc1 @tc2
Scenario Outline: Validate that <link> link opens correctly
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
         
     Examples:
        | link                             | url                             | inputText                         |
        | ES.@tc1.ContactLink              | ES.@tc1.ContactUrl              | ES.@tc1.ContactInputText          |
        | ES.@tc1.ProductsLink             | ES.@tc1.ProductsUrl             | ES.@tc1.ProductsInputText         |
        | ES.@tc1.ResourceLibraryLink      | ES.@tc1.ResourceLibraryUrl      | ES.@tc1.ResourceLibraryInputText  |
        | ES.@tc1.NewsEventsLink           | ES.@tc1.NewsEventsUrl           | ES.@tc1.NewsEventsInputText       |
        | ES.@tc1.AboutGoreLink            | ES.@tc1.AboutGoreUrl            | ES.@tc1.AboutGoreInputText        |
        | ES.@tc1.CareersLink              | ES.@tc1.CareersUrl              | ES.@tc1.CareersInputText          |
        | ES.@tc1.PrivacyNoticeLink        | ES.@tc1.PrivacyNoticeUrl        | ES.@tc1.PrivacyNoticeInputText    |
        | ES.@tc1.TermsOfUseLink           | ES.@tc1.TermsOfUseUrl           | ES.@tc1.TermsOfUseInputText       |
        | ES.@tc1.CookiesLink              | ES.@tc1.CookiesUrl              | ES.@tc1.CookiesInputText          |
        | UK.@tc1.ModernSlaveryActLinkText | UK.@tc1.ModernSlaveryActLinkUrl | UK.@tc1.ModernSlaveryActInputText |
        | ES.@tc1.LegalNoteLink            | ES.@tc1.LegalNoteUrl            | ES.@tc1.LegalNoteInputText        |
    

@tc2.1
Scenario: Validate that Youtube link opens correctly
        When I click on the link "YouTube"
        Then I expect the url "youtube.com/user/wlgoreassociates" is opened in a new tab

@tc3
Scenario: Validate Carousel on Homepage
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "ES.@tc3.carouselTitle1"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "ES.@tc3.carouselTitle2"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "ES.@tc3.carouselTitle1"
       
@tc3.1
Scenario: Validate that Carousel Learn More links opens correctly
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'ES.@tc3.1.LearnMoreLink')]"
        Then I expect that the url is "CH.@tc3.1.CaroselLink1"
        And I expect that element "body" contains the text "ES.@tc3.1.carouselText1"
        When I navigate back in browser
        When I follow prev link on hero bar
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'ES.@tc3.1.LearnMoreLink')]"
        Then I expect that the url is "UK.@tc3.1.CarouselUrl"
        And I expect that element "body" contains the text "ES.@tc3.1.carouselText2"

@tc4
Scenario Outline: Validate that gore main page appears <youtubeId> youtube videos
        When I click on the "//a[contains(@href,'<youtubeId>')]"
        Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
            
    Examples: 
        | youtubeId          |
        | ES.@tc4.YoutubeId1 |
        | US.@tc4.YoutubeId2 |
      
@tc5
Scenario Outline: Validate that <link> product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
        When I click on the link "ES.@tc1.ProductsLink"
        Then I expect that the url is "ES.@tc1.ProductsUrl"
        When I click on the "//h3/a[contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"

    Examples:
        | link                             | url                             | inputText                             |
        | ES.@tc5.ConsumerProductsLink     | ES.@tc5.ConsumerProductsUrl     | ES.@tc5.ConsumerProductsInputText     |
        | ES.@tc5.CablesLink               | ES.@tc5.CablesUrl               | ES.@tc5.CablesInputText               |
        | ES.@tc5.ElectronicComponentsLink | ES.@tc5.ElectronicComponentsUrl | ES.@tc5.ElectronicComponentsInputText |
        | ES.@tc5.FabricsLink              | ES.@tc5.FabricsUrl              | ES.@tc5.FabricsInputText              |
        | ES.@tc5.FibersLink               | ES.@tc5.FibersUrl               | ES.@tc5.FibersInputText               |
        | ES.@tc5.FiltrationLink           | ES.@tc5.FiltrationUrl           | ES.@tc5.FiltrationInputText           |
        | ES.@tc5.MedicalLink              | ES.@tc5.MedicalUrl              | ES.@tc5.MedicalInputText              |
        | ES.@tc5.PharmaceuticalLink       | US.@tc5.PharmaceuticalLinkUrl   | ES.@tc5.PharmaceuticalInputText       |
        | ES.@tc5.SealantsLink             | ES.@tc5.SealantsUrl             | ES.@tc5.SealantsInputText             |
        | ES.@tc5.VentingLink              | ES.@tc5.VentingUrl              | ES.@tc5.VentingInputText              |

@tc6 @tc7
Scenario: Validate that main page appears texts and links
        When I click on the link "ES.@tc6.MoreAboutGore"
        Then I expect that the url is "ES.@tc6.MoreAboutGoreUrl"
        And the title is "ES.@tc6.AboutGoreTitle"
        When I navigate back in browser
        And I click on the link "ES.@tc6.MoreTechnologies"
        Then I expect that the url is "ES.@tc6.MoreTechnologiesUrl"
        And the title is "US.@tc6.MoreTechnologiesTitle"
        When I navigate back in browser
        And I click on the link "ES.@tc6.ExploreTechnologies"
        Then I expect that the url is "ES.@tc6.ExploreTechnologiesUrl"
        And the title is "US.@tc6.ExploreTechnologiesTitle"
        When I navigate back in browser
        Then I expect that element "body" contains the text "ES.@tc1.NewsEventsLink"
        And I expect that element "body" contains the text "ES.@tc6.InputText1"
        And I expect that element "body" contains the text "ES.@tc6.InputText2"
        And I expect that element "body" contains the text "ES.@tc6.InputText3"


@tc8 @tc11
Scenario Outline: Validate that the View Gore Products By <industry> works correctly
        When I click on the "//a[contains(text(),'ES.@tc8.ViewGoreProducts')]"
        And I scroll to element "//div[@class='by-industry__list']//a[contains(text(),'<industry>')]"
        And I click on the link "<industry>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        When I click on the link "ES.@tc1.ProductsLink"
        And I click on the link "ES.@tc8.IndustryLink"
        And I click on the "//h3/a[contains(text(),'<industry>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
          
    Examples:
        | industry                          | url                          |
        | ES.@tc8.AerospaceIndustry         | ES.@tc8.AerospaceUrl         |   
        | ES.@tc8.ApparelTextilesIndustry   | ES.@tc8.ApparelTextilesUrl   |
        | ES.@tc8.AutomotiveIndustry        | ES.@tc8.AutomotiveUrl        |
        | ES.@tc8.ChemicalsIndustry         | ES.@tc8.ChemicalsUrl         |
        | ES.@tc8.EnvProtectionIndustry     | ES.@tc8.EnvProtectionUrl     |
        | ES.@tc8.FireSafetyIndustry        | ES.@tc8.FireSafetyUrl        |
        | ES.@tc8.IndustrialIndustry        | ES.@tc8.IndustrialUrl        |
        | ES.@tc8.LifeSciencesIndustry      | US.@tc8.LifeSciencesUrl      |
        | ES.@tc8.MilitaryIndustry          | ES.@tc8.MilitaryUrl          |
        | ES.@tc8.MobileElectronicsIndustry | ES.@tc8.MobileElectronicsUrl |
        | ES.@tc8.OilGasIndustry            | ES.@tc8.OilGasUrl            |
        | ES.@tc8.PowerUtilitiesIndustry    | ES.@tc8.PowerUtilitiesUrl    |
        | ES.@tc8.SemiconductorIndustry     | ES.@tc8.SemiconductorUrl     |
        | ES.@tc8.TechnologyIndustry        | ES.@tc8.TechnologyUrl        |
        | ES.@tc8.TestMeasurementIndustry   | ES.@tc8.TestMeasurementUrl   |


@tc9
Scenario: Validate that Product page appears texts and links
        When I click on the link "ES.@tc1.ProductsLink"
        Then I expect that the url is "ES.@tc1.ProductsUrl"
        And the title is "ES.@tc9.ProductsTitle"
        And I expect that element "body" contains the text "ES.@tc9.ProductsInputText1"
        And I expect that element "body" contains the text "ES.@tc9.ProductsInputText2"
        And I expect that element "body" contains the text "ES.@tc9.ProductsInputText3"


@tc10
Scenario: Validate that View all products page appear product
        When I click on the link "ES.@tc1.ProductsLink"
        And I click on the link "ES.@tc10.ViewAllProductsLink"
        And I expect that element "//h2" contains the text "ES.@tc1.ProductsLink"
        When I set href to sharedContext from '//div[@class="column products-large-list"][3]//a' element
        And I click on the "//div[@class='column products-large-list'][3]//a"
        Then I expect the url "contextText" is opened in a new tab


@tc13 @tc14
Scenario: Validate that Resources search page appears texts and links
        When I click on the link "ES.@tc1.ResourceLibraryLink"
        Then I expect that element "//button[contains(text(), 'ES.@tc13.ResouresViewResults')]" is displayed
        When I click on the button "ES.@tc13.ResouresViewResults"
        Then I expect that the url is "ES.@tc13.ResouresViewResultsUrl"
        And the title is "ES.@tc13.ResouresViewResultsTitle"
        And I expect that element "body" contains the text "ES.@tc1.ResourceLibraryLink"
        And I expect that element "body" contains the text "ES.@tc1.ResourceLibraryInputText"
        And I expect that element "body" contains the text "ES.@tc13.ResouresViewResultsInputText1"
        And I expect that element "body" contains the text "ES.@tc13.ResouresViewResultsInputText2"
        And I expect that element "body" contains the text "ES.@tc13.ResouresViewResultsInputText3"

@tc15
Scenario: Validate that Resources search page appear tabs 
        When I click on the link "ES.@tc1.ResourceLibraryLink"
        And I click on the button "ES.@tc13.ResouresViewResults"
        Then I expect that the url is "ES.@tc13.ResouresViewResultsUrl"
        And I expect that element "//div[@class='tabs']//a[contains(., 'ES.@tc15.LanguageTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'ES.@tc15.ContentTyoeTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'ES.@tc15.CategoryTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'ES.@tc15.IndustryTab')]" is displayed

@tc27
Scenario: Validate that Search page works correctly
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "tigris" to the inputfield "//input[@type='search']"
        And I click on the button "ES.@tc27.SearchButton"
        Then I expect that element "body" contains the text "ES.@tc27.SearchResultsInputText1"
        And I expect that element "body" contains the text "ES.@tc27.SearchResultsInputText2"