Feature: goreComIT 
    Check gore.com main page functionality with IT location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'Italia')]"
        Then I expect that the url is "gore.it"

@tc17
Scenario: Validate that the filtering by "Content Type" works correctly
        When I click on the link "IT.@tc17.ResourceLibraryLink"
        And I click on the "//input[@value='content_type:326']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'IT.@tc17.AuthorizedDistributorsFilter')]" is displayed
        When I click on the button "IT.@tc17.ViewResourceResultsLink"
        Then I expect that element "body" contains the text "IT.@tc17.AuthorizedDistributorsInputText1"
        And I expect that element "body" contains the text "IT.@tc17.AuthorizedDistributorsFilter"
        And I expect that element "body" contains the text "IT.@tc17.AuthorizedDistributorsInputText2"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'IT.@tc17.ContentTypeTab')]/span[contains(@class,'tab-active')]" is displayed
  
@tc29
Scenario: Validate that Consumer Products page appears texts and link
        When I click on the link "IT.@tc29.ProductLink"
        And I click on the "//h3/a[contains(text(),'IT.@tc29.ConsumerProductsLink')]"
        Then I expect that the url is "IT.@tc29.ConsumerProductsUrl" 
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsLink"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText1"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText2"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText3"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText4"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText4"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText5"
        And I expect that element "body" contains the text "IT.@tc29.ConsumerProductsInputText6"
        When I click on the link "IT.@tc29.ViewAllIndustriesAndCategoriesLink"
        Then I expect that the url is "IT.@tc29.ProductUrl"

@tc29.1
Scenario Outline: Validate that Consumer Products page appears links that opened a new tab
        When I click on the link "IT.@tc29.ProductLink"
        And I click on the "//h3/a[contains(text(),'IT.@tc29.ConsumerProductsLink')]"
        Then I expect that the url is "IT.@tc29.ConsumerProductsUrl" 
        When I click on the link "<link>"
        Then I expect the url "<url>" is opened in a new tab
  
    Examples:
        | link                                 | url                                 |     
        | IT.@tc29.1.VisitGoreTexComLink       | IT.@tc29.1.VisitGoreTexComUrl       |
        | IT.@tc29.1.VisitWindstopperComLink   | US.@tc29.1.VisitWindstopperComUrl   |
        | IT.@tc29.1.VisitGoreWearComLink      | IT.@tc29.1.VisitGoreWearComUrl      |
        | IT.@tc29.1.VisitOptifadeComLink      | US.@tc29.1.VisitOptifadeComUrl      |
        | IT.@tc29.1.VisitElixirstringsComLink | US.@tc29.1.VisitElixirstringsComUrl |