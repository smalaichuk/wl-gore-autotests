Feature: goreComDE 
    Check gore.com main page functionality with DE location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'Deutschland')]"
        Then I expect that the url is "gore.de"

@tc1 @tc2
Scenario Outline: Validate that <link> link opens correctly
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
         
     Examples:
        | link                             | url                             | inputText                         |
        | DE.@tc1.ContactLink              | DE.@tc1.ContactUrl              | DE.@tc1.ContactInputText          |
        | DE.@tc1.ProductsLink             | DE.@tc1.ProductsUrl             | DE.@tc1.ProductsInputText         |
        | DE.@tc1.ResourcesLink            | US.@tc1.ResourceLibraryLinkUrl  | DE.@tc1.ResourcesInputText        |
        | US.@tc1.NewsEventsLinkText       | US.@tc1.NewsEventsLinkUrl       | DE.@tc1.NewsInputText             |
        | DE.@tc1.AboutGoreLink            | DE.@tc1.AboutGoreUrl            | DE.@tc1.AboutGoreInputText        |
        | DE.@tc1.CareersLink              | DE.@tc1.CareersUrl              | DE.@tc1.CareersInputText          |
        | DE.@tc1.PrivacyNoticeLink        | DE.@tc1.PrivacyNoticeUrl        | DE.@tc1.PrivacyNoticeInputText    |
        | DE.@tc1.TermsOfUseLink           | DE.@tc1.TermsOfUseUrl           | DE.@tc1.TermsOfUseInputText       | 
        | DE.@tc1.CookiesLink              | DE.@tc1.CookiesUrl              | DE.@tc1.CookiesInputText          |
        | UK.@tc1.ModernSlaveryActLinkText | UK.@tc1.ModernSlaveryActLinkUrl | UK.@tc1.ModernSlaveryActInputText |
        | DE.@tc1.ImprintLink              | DE.@tc1.ImprintUrl              | DE.@tc1.ImprintInputText          |
    

@tc2.1
Scenario: Validate that Youtube link opens correctly
        When I click on the link "YouTube"
        Then I expect the url "youtube.com/user/wlgoreassociates" is opened in a new tab

@tc3
Scenario: Validate Carousel on Homepage
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "DE.@tc3.carouselTitle1"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "DE.@tc3.carouselTitle2"
        When I follow prev link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "DE.@tc3.carouselTitle1"
       
@tc3.1
Scenario: Validate that Carousel Learn More links opens correctly
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'Weitere Informationen')]"
        Then I expect that the url is "CH.@tc3.1.CarouselLink2"
        And I expect that element "body" contains the text "US.@tc3.1.carouselText1"
        When I navigate back in browser
        When I follow next link on hero bar
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'Weitere Informationen')]"
        Then I expect that the url is "CH.@tc3.1.CaroselLink1"
        And I expect that element "body" contains the text "DE.@tc3.1.carouselText1"

@tc4
Scenario Outline: Validate that gore main page appears <youtubeId> youtube videos
        When I click on the "//a[contains(@href,'<youtubeId>')]"
        Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
    
    Examples: 
        | youtubeId          |
        | DE.@tc4.YoutubeId1 |
        | US.@tc4.YoutubeId2 |
      
@tc5
Scenario Outline: Validate that <link> product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
        When I click on the link "DE.@tc1.ProductsLink"
        Then I expect that the url is "DE.@tc1.ProductsUrl"
        When I click on the "//h3/a[contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"

    Examples:
        | link                             | url                             | inputText                             |
        | DE.@tc5.ConsumerProductsLink     | DE.@tc5.ConsumerProductsUrl     | DE.@tc5.ConsumerProductsInputText     |
        | DE.@tc5.CablesLink               | DE.@tc5.CablesUrl               | DE.@tc5.CablesInputText               |
        | DE.@tc5.ElectronicComponentsText | DE.@tc5.ElectronicComponentsUrl | DE.@tc5.ElectronicComponentsInputText |
        | DE.@tc5.TextilLink               | DE.@tc5.TextilUrl               | DE.@tc5.TextilInputText               |
        | DE.@tc5.FibersLink               | DE.@tc5.FibersUrl               | DE.@tc5.FibersInputText               |
        | DE.@tc5.FiltrationLink           | DE.@tc5.FiltrationUrl           | DE.@tc5.FiltrationInputText           |
        | DE.@tc5.MedicalLink              | DE.@tc5.MedicalUrl              | DE.@tc5.MedicalInputText              |
        | DE.@tc5.PharmaceuticalLink       | US.@tc5.PharmaceuticalLinkUrl   | DE.@tc5.PharmaceuticalInputText       |
        | DE.@tc5.SealantsLink             | DE.@tc5.SealantsUrl             | DE.@tc5.SealantsInputText             |
        | DE.@tc5.VentingLink              | DE.@tc5.VentingUrl              | DE.@tc5.VentingInputText              |

@tc6 @tc7
Scenario: Validate that main page appears texts and links
        When I click on the link "DE.@tc6.MoreTechnologies"
        Then I expect that the url is "DE.@tc6.MoreTechnologiesUrl"
        And the title is "US.@tc6.MoreTechnologiesTitle"
        When I navigate back in browser
        And I click on the link "DE.@tc6.MoreAboutGore"
        Then I expect that the url is "DE.@tc6.MoreAboutGoreUrl"
        And the title is "DE.@tc6.AboutGoreTitle"
        When I navigate back in browser
        And I click on the link "DE.@tc6.ExploreTechnologies"
        Then I expect that the url is "DE.@tc6.ExploreTechnologiesUrl"
        And the title is "US.@tc6.ExploreTechnologiesTitle"
        When I navigate back in browser
        Then I expect that element "body" contains the text "US.@tc6.InputText1"
        And I expect that element "body" contains the text "DE.@tc6.InputText1"
        And I expect that element "body" contains the text "DE.@tc6.InputText2"
        And I expect that element "body" contains the text "DE.@tc6.InputText3"

@tc8 @tc11
Scenario Outline: Validate that the View Gore Products By <industry> works correctly
        When I click on the "//a[contains(text(),'DE.@tc8.ViewGoreProducts')]"
        And I scroll to element "//div[@class='by-industry__list']//a[contains(text(),'<industry>')]"
        And I click on the link "<industry>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        When I click on the link "DE.@tc1.ProductsLink"
        And I click on the link "DE.@tc8.IndustryLink"
        And I click on the "//h3/a[contains(text(),'<industry>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
          
    Examples:
        | industry                          | url                          |
        | DE.@tc8.AerospaceIndustry         | DE.@tc8.AerospaceUrl         |   
        | DE.@tc8.ApparelTextilesIndustry   | DE.@tc8.ApparelTextilesUrl   |
        | DE.@tc8.AutomotiveIndustry        | DE.@tc8.AutomotiveUrl        |
        | DE.@tc8.ChemicalsIndustry         | DE.@tc8.ChemicalsUrl         |
        | DE.@tc8.EnvProtectionIndustry     | DE.@tc8.EnvProtectionUrl     |
        | DE.@tc8.FireSafetyIndustry        | DE.@tc8.FireSafetyUrl        |
        | DE.@tc8.IndustrialIndustry        | DE.@tc8.IndustrialUrl        |
        | DE.@tc8.LifeSciencesIndustry      | DE.@tc8.LifeSciencesUrl      |
        | DE.@tc8.MilitaryIndustry          | DE.@tc8.MilitaryUrl          |
        | DE.@tc8.MobileElectronicsIndustry | DE.@tc8.MobileElectronicsUrl |
        | DE.@tc8.OilGasIndustry            | DE.@tc8.OilGasUrl            |
        | DE.@tc8.PowerUtilitiesIndustry    | DE.@tc8.PowerUtilitiesUrl    |
        | DE.@tc8.SemiconductorIndustry     | DE.@tc8.SemiconductorUrl     |
        | DE.@tc8.TechnologyIndustry        | DE.@tc8.TechnologyUrl        |
        | DE.@tc8.TestMeasurementIndustry   | DE.@tc8.TestMeasurementUrl   |

@tc9
Scenario: Validate that Product page appears texts and links
        When I click on the link "DE.@tc1.ProductsLink"
        Then I expect that the url is "DE.@tc1.ProductsUrl"
        And the title is "DE.@tc9.ProductsTitle"
        And I expect that element "body" contains the text "DE.@tc1.ProductsLink"
        And I expect that element "body" contains the text "DE.@tc1.ProductsInputText"
        And I expect that element "body" contains the text "DE.@tc9.ProductsInputText1"
        And I expect that element "body" contains the text "DE.@tc9.ProductsInputText2"

@tc10
Scenario: Validate that View all products page appear product
        When I click on the link "DE.@tc1.ProductsLink"
        When I click on the link "DE.@tc10.ViewAllProductsLink"
        Then I expect that the url is "US.@tc10.ViewAllProductsUrl"
        And the title is "DE.@tc10.ViewAllProductsTitle"
        And I expect that element "//h2" contains the text "DE.@tc1.ProductsLink"
        When I set href to sharedContext from '//div[@class="column products-large-list"][3]//a' element
        And I click on the "//div[@class='column products-large-list'][3]//a"
        Then I expect the url "contextText" is opened in a new tab

@tc13 @tc14
Scenario: Validate that Resources search page appears texts and links
        When I click on the link "DE.@tc1.ResourcesLink"
        Then I expect that element "//button[contains(text(), 'DE.@tc13.ResouresViewResults')]" is displayed
        When I click on the button "DE.@tc13.ResouresViewResults"
        Then I expect that the url is "DE.@tc13.ResouresViewResultsUrl"
        And the title is "DE.@tc13.ResouresViewResultsTitle"
        And I expect that element "body" contains the text "DE.@tc1.ResourcesLink"
        And I expect that element "body" contains the text "DE.@tc1.ResourcesInputText"
        And I expect that element "body" contains the text "DE.@tc13.ResouresViewResultsInputText1"
        And I expect that element "body" contains the text "DE.@tc13.ResouresViewResultsInputText2"
        And I expect that element "body" contains the text "DE.@tc13.ResouresViewResultsInputText3"

@tc15
Scenario: Validate that Resources search page appear tabs 
        When I click on the link "DE.@tc1.ResourcesLink"
        When I click on the button "DE.@tc13.ResouresViewResults"
        Then I expect that the url is "DE.@tc13.ResouresViewResultsUrl"
        And I expect that element "//div[@class='tabs']//a[contains(., 'DE.@tc15.LanguageTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'DE.@tc15.ContentTyoeTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'DE.@tc15.CategoryTab')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'DE.@tc15.IndustryTab')]" is displayed

@tc27
Scenario: Validate that Search page works correctly
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "tigris" to the inputfield "//input[@type='search']"
        And I click on the button "DE.@tc27.SearchButton"
        Then I expect that element "body" contains the text "DE.@tc27.SearchResultsInputText1"
        And I expect that element "body" contains the text "DE.@tc27.SearchResultsInputText2"