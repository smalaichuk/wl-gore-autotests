Feature: goreComVRTAreas
    Check gore.com main page areas for locations

    Background: Open GoreCom
        Given I open "HomePage" page
        Then I expect that the url is "US.goreComLink"
        
    @tc1 @visual
    Scenario: Check header for US location of gore.com
        Then I expect that the element "//header[@class='site-header']" screenshot 'goreComUSHeader' is equal to baseline

    @tc2 @visual
    Scenario: Check footer for US location of gore.com
        When I scroll to element "//div[@class='layout-split-2']"
        Then I expect that the element "//div[@class='layout-split-2']" screenshot 'goreComUSFooter' is equal to baseline

    @tc3 @visual
    Scenario: Check careers area for US location of gore.com
        When I scroll to element "//section[@class='careers-feature spacing-top-double']"
        Then I expect that the element "//section[@class='careers-feature spacing-top-double']" screenshot 'goreComUSCareers' is equal to baseline 
        
    @tc4 @visual
    Scenario: Check products area for US location of gore.com
        When I scroll to element "//div[@data-linkcontainer='lt_region' and @data-tracklinktext='inline']"
        Then I expect that the element "//div[@data-linkcontainer='lt_region' and @data-tracklinktext='inline']" screenshot 'goreComUSProducts' is equal to baseline 

        