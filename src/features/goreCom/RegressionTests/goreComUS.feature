Feature: goreComUS 
    Check gore.com functionality with US location

    Background: Open GoreCom
        Given I open "HomePage" page
        Then I expect that the url is "gore.com"

@tc3
Scenario: Validate Carousel on Homepage
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle1"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle2"
        When I follow next link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle3"
        When I follow prev link on hero bar
        Then I expect that element "//div[contains(@class,'slick-active')]//h2" contains the text "US.@tc3.carouselTitle2"
     
@tc3.1
Scenario: Validate that Carousel Learn More links opens correctly
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'Learn more')]"
        Then I expect that the url is "US.@tc3.1.carouselLink1"
        And I expect that element "body" contains the text "US.@tc3.1.carouselText1"
        When I navigate back in browser
        When I follow prev link on hero bar
        When I click on the "//div[contains(@class,'slick-active')]//a[contains(.,'Learn more')]"
        Then I expect that the url is "US.@tc3.1.carouselLink2"
        And I expect that element "body" contains the text "US.@tc3.1.carouselText2"

@tc5
Scenario Outline: Validate that <link> product link opens correctly
        When I click on the "//div[@class='category-block__title' and contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
        When I click on the link "US.@tc1.ProductsLinkText"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        When I click on the "//h3/a[contains(text(),'<link>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<inputText>"
      Examples:
        | link                                 | url                                 | inputText                             |
        | US.@tc5.ConsumerProductsLinkText     | US.@tc5.ConsumerProductsLinkUrl     | US.@tc5.ConsumerProductsInputText     |
        | US.@tc5.CablesLinkText               | US.@tc5.CablesLinkUrl               | US.@tc5.CablesInputText               |
        | US.@tc5.ElectronicComponentsLinkText | US.@tc5.ElectronicComponentsLinkUrl | US.@tc5.ElectronicComponentsInputText |
        | US.@tc5.FabricsLinkText              | US.@tc5.FabricsLinkUrl              | US.@tc5.FabricsInputText              |
        | US.@tc5.FibersLinkText               | US.@tc5.FibersLinkUrl               | US.@tc5.FibersInputText               |
        | US.@tc5.FiltrationLinkText           | US.@tc5.FiltrationLinkUrl           | US.@tc5.FiltrationInputText           |
        | US.@tc5.MedicalLinkText              | US.@tc5.MedicalLinkUrl              | US.@tc5.MedicalInputText              |
        | US.@tc5.PharmaceuticalLinkText       | US.@tc5.PharmaceuticalLinkUrl       | US.@tc5.PharmaceuticalInputText       |
        | US.@tc5.SealantsLinkText             | US.@tc5.SealantsLinkUrl             | US.@tc5.SealantsInputText             |
        | US.@tc5.VentingLinkText              | US.@tc5.VentingLinkUrl              | US.@tc5.VentingInputText              |

@tc8 @tc11
Scenario Outline: Validate that the View Gore Products By <industry> works correctly
        When I click on the "//a[contains(text(),'US.@tc8.ViewGoreProducts')]"
        And I scroll to element "//div[@class='by-industry__list']//a[contains(text(),'<industry>')]"
        And I click on the link "<industry>"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
        When I click on the link "Products"
        And I click on the link "Industries"
        And I click on the "//h3/a[contains(text(),'<industry>')]"
        Then I expect that the url is "<url>"
        And I expect that element "body" contains the text "<industry>"
          
    Examples:
        | industry                          | url                          |
        | US.@tc8.AerospaceIndustry         | US.@tc8.AerospaceUrl         |
        | US.@tc8.ApparelTextilesIndustry   | US.@tc8.ApparelTextilesUrl   |
        | US.@tc8.AutomotiveIndustry        | US.@tc8.AutomotiveUrl        |
        | US.@tc8.ChemicalsIndustry         | US.@tc8.ChemicalsUrl         |
        | US.@tc8.EnvProtectionIndustry     | US.@tc8.EnvProtectionUrl     |
        | US.@tc8.FireSafetyIndustry        | US.@tc8.FireSafetyUrl        |
        | US.@tc8.IndustrialIndustry        | US.@tc8.IndustrialUrl        |
        | US.@tc8.LifeSciencesIndustry      | US.@tc8.LifeSciencesUrl      |
        | US.@tc8.MilitaryIndustry          | US.@tc8.MilitaryUrl          |
        | US.@tc8.MobileElectronicsIndustry | US.@tc8.MobileElectronicsUrl |
        | US.@tc8.OilGasIndustry            | US.@tc8.OilGasUrl            |
        | US.@tc8.PowerUtilitiesIndustry    | US.@tc8.PowerUtilitiesUrl    |
        | US.@tc8.SemiconductorIndustry     | US.@tc8.SemiconductorUrl     |
        | US.@tc8.TechnologyIndustry        | US.@tc8.TechnologyUrl        |
        | US.@tc8.TestMeasurementIndustry   | US.@tc8.TestMeasurementUrl   |

@tc10
Scenario: Validate that View all products page appears some products
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the link "US.@tc10.ViewAllProductsLink"
        Then I expect that the url is "US.@tc10.ViewAllProductsUrl"
        And I expect that element "//h2" contains the text "US.@tc1.ProductsLinkText"
        When I set text to sharedContext from '//div[@class="column products-large-list"][1]//a' element
        And I click on the "//div[@class='column products-large-list'][1]//a"
        Then I expect that element "body" contains the text "contextText"
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the link "US.@tc10.ViewAllProductsLink"
        And I set text to sharedContext from '//div[@class="column products-large-list"][2]//a' element
        And I click on the "//div[@class='column products-large-list'][2]//a"
        Then I expect that element "body" contains the text "contextText"

@tc12
Scenario: Validate that Resources page appears texts and links
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        Then I expect that the url is "US.@tc1.ResourceLibraryLinkUrl"
        And the title is "US.@tc12.ResourceLibraryTitle"
        And I expect that element "body" contains the text "US.@tc1.ResourceLibraryLinkText"
        And I expect that element "body" contains the text "US.@tc1.ResourceLibraryInputText"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText1"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText2"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText3"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText4"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText5"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText6"

@tc13 @tc14
Scenario: Validate that Resources search page appears texts and links
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        Then I expect that element "//button[contains(text(), 'US.@tc13.ViewResultsButton')]" is displayed
        When I click on the button "US.@tc13.ViewResultsButton"
        Then I expect that the url is "US.@tc13.ResourcesSearchLink"
        And the title is "US.@tc12.ResourceLibraryTitle"
        And I expect that element "body" contains the text "US.@tc1.ResourceLibraryLinkText"
        And I expect that element "body" contains the text "US.@tc1.ResourceLibraryInputText"
        And I expect that element "body" contains the text "US.@tc13.ResourceResultsInputText1"
        And I expect that element "body" contains the text "US.@tc13.ResourceResultsInputText2"
        And I expect that element "body" contains the text "US.@tc13.ResourceResultsInputText3"

@tc15
Scenario: Validate that Resources search page appear tabs 
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        And I click on the button "US.@tc13.ViewResultsButton"
        Then I expect that the url is "US.@tc13.ResourcesSearchLink"
        And I expect that element "//div[@class='tabs']//a[contains(., 'US.@tc12.ResourceLibraryInputText3')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'US.@tc12.ResourceLibraryInputText4')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'US.@tc15.ResourceResultsInputText1')]" is displayed
        And I expect that element "//div[@class='tabs']//a[contains(., 'US.@tc15.ResourceResultsInputText2')]" is displayed


@tc17
Scenario: Validate that the filtering by "Content Type" works correctly
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        And I click on the "//input[@value='content_type:326']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'US.@tc17.AuthorizedDistributorsFilter')]" is displayed
        When I click on the button "US.@tc13.ViewResultsButton"
        Then I expect that element "body" contains the text "US.@tc17.AuthorizedDistributorsInputText1"
        And I expect that element "body" contains the text "US.@tc17.AuthorizedDistributorsInputText2"
        And I expect that element "body" contains the text "US.@tc17.AuthorizedDistributorsInputText3"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'US.@tc12.ResourceLibraryInputText4')]/span[contains(@class,'tab-active')]" is displayed
  
@tc18
Scenario: Validate that the filtering by "Categories" works correctly
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        And I click on the "//legend/..//a[contains(.,'US.@tc5.CablesLinkText')]"
        And I click on the "//legend/..//input[@value='categories:1006']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'US.@tc8.AerospaceIndustry')]" is displayed
        When I click on the button "US.@tc13.ViewResultsButton"
        Then I expect that element "body" contains the text "US.@tc18.AerospaceInputText1"
        And I expect that element "body" contains the text "US.@tc18.AerospaceInputText2"
        And I expect that element "body" contains the text "US.@tc18.AerospaceInputText3"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'US.@tc15.ResourceResultsInputText1')]/span[contains(@class,'tab-active')]" is displayed

@tc19
Scenario: Validate that the filtering by "Industries" works correctly
        When I click on the link "US.@tc1.ResourceLibraryLinkText"
        And I click on the "//input[@value='industries:666']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'US.@tc8.ApparelTextilesIndustry')]" is displayed
        When I click on the button "US.@tc13.ViewResultsButton"
        Then I expect that element "body" contains the text "US.@tc19.IndustriesInputText1"
        And I expect that element "body" contains the text "US.@tc19.IndustriesInputText2"
        And I expect that element "body" contains the text "US.@tc19.IndustriesInputText3"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'US.@tc15.ResourceResultsInputText2')]/span[contains(@class,'tab-active')]" is displayed

@tc20
Scenario: Validate that News & Events page appears texts
        When I click on the link "US.@tc1.NewsEventsLinkText"
        Then I expect that the url is "US.@tc1.NewsEventsLinkUrl"
        And the title is "US.@tc20.NewsEventsTitle"
        And I expect that element "body" contains the text "US.@tc1.NewsEventsLinkText"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText1"
        And I expect that element "body" contains the text "US.@tc13.ResourceResultsInputText2"
        And I expect that element "body" contains the text "US.@tc15.ResourceResultsInputText2"
        And I expect that element "body" contains the text "US.@tc15.ResourceResultsInputText1"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText4"
        And I expect that element "body" contains the text "US.@tc12.ResourceLibraryInputText3"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText2"
        And I expect that element "body" contains the text "US.@tc1.AboutGoreLinkText"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText3"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText4"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText5"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText6"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText7"

@tc21
Scenario Outline: Validate that News & Events page appears <link> link
        When I click on the link "US.@tc1.NewsEventsLinkText"
        Then I expect that the url is "US.@tc1.NewsEventsLinkUrl"
        And the title is "US.@tc20.NewsEventsTitle"
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
  
    Examples:
        | link                            | url                            |
        | US.@tc20.NewsEventsInputText2   | US.@tc21.MediaContactsUrl      |
        | US.@tc21.PressReleasesLink      | US.@tc21.PressReleasesUrl      |
        | US.@tc21.NewsLink               | US.@tc21.NewsUrl               |
        | US.@tc21.ImagesLink             | US.@tc21.ImagesUrl             |
        | US.@tc21.EnterprisePressKitLink | US.@tc21.EnterprisePressKitUrl |
        | US.@tc21.SeeMoreLink            | US.@tc21.SeeMoreUrl            |

@tc21.1
Scenario: Validate that News & Events page appears Load more link
        When I click on the link "US.@tc1.NewsEventsLinkText"
        Then I expect that the url is "US.@tc1.NewsEventsLinkUrl"
        And the title is "US.@tc20.NewsEventsTitle"
        And I set count of '//div[@class="column"]' elements to sharedContext
        When I click on the link "US.@tc21.1.LoadMoreNewsLink"
        Then The count of '//div[@class="column"]' elements not equal to 'contextLength' 

@tc22
Scenario: Validate that About Gore page appears texts
        When I click on the link "US.@tc1.AboutGoreLinkText"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        And the title is "US.@tc6.AboutGoreTitle"
        And I expect that element "body" contains the text "US.@tc1.AboutGoreLinkText"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText1"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText2"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText3"
        And I expect that element "body" contains the text "US.@tc1.NewsEventsLinkText"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText4"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText5"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText5"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText6"

@tc23
Scenario Outline: Validate that About page appears <link> link
        When I click on the link "US.@tc1.AboutGoreLinkText"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        And the title is "US.@tc6.AboutGoreTitle"
        When I click on the link "<link>"
        Then I expect that the url is "<url>"
  
    Examples:
        | link                          | url                          |
        | US.@tc23.TheGoreStoryLink     | US.@tc23.TheGoreStoryUrl     |
        | US.@tc23.TechnologiesLink     | US.@tc23.TechnologiesUrl     |
        | US.@tc23.CompanyCultureLink   | US.@tc23.CompanyCultureUrl   |
        | US.@tc23.FastFactsLink        | US.@tc23.FastFactsUrl        |
        | US.@tc23.TheEptfeStoryLink    | US.@tc23.TheEptfeStoryUrl    |
        | US.@tc23.OurBeliefsLink       | US.@tc23.OurBeliefsUrl       |
        | US.@tc23.MoreAboutCareersLink | US.@tc23.MoreAboutCareersUrl |

@tc23.1
Scenario Outline: Validate that About page appears links that opens new tab
        When I click on the link "US.@tc1.AboutGoreLinkText"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        And the title is "US.@tc6.AboutGoreTitle"
        When I click on the link "<link>"
        Then I expect the url "<url>" is opened in a new tab
  
    Examples:
        | link                                  | url                                  |       
        | US.@tc23.1.GoreTexOutwearLink         | US.@tc23.1.GoreTexOutwearUrl         |
        | US.@tc23.1.GoreCleanstreamFiltersLink | US.@tc23.1.GoreCleanstreamFiltersUrl |

@tc24
Scenario: Validate that Careers page appears texts
        When I click on the link "US.@tc1.CareersLinkText"
        Then I expect that the url is "US.@tc1.CareersLinkUrl"
        And the title is "US.@tc24.CareersTitle"
        And I expect that element "body" contains the text "US.@tc1.CareersLinkText"
        And I expect that element "body" contains the text "US.@tc1.AboutGoreLinkText"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText1"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText2"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText3"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText4"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText5"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText6"
        And I expect that element "body" contains the text "US.@tc24.CareersInputText7"

@tc25
Scenario Outline: Validate that Careers page appears <link> link
        When I click on the link "US.@tc1.CareersLinkText"
        Then I expect that the url is "US.@tc1.CareersLinkUrl"
        And the title is "US.@tc24.CareersTitle"
        When I click on the link "<link>"
        Then I expect that the url is "<url>"

    Examples:
        | link                        | url                        | 
        | US.@tc25.AwardsLink         | US.@tc25.AwardsUrl         |
        | US.@tc25.GoreLocationsLink  | US.@tc25.GoreLocationsUrl  |
        | US.@tc25.BenefitsLink       | US.@tc25.BenefitsUrl       |
        | US.@tc25.CompanyCultureLink | US.@tc23.CompanyCultureUrl |    

@tc25.1
Scenario Outline: Validate that Careers page appears <link> link that opens new tab
        When I click on the link "US.@tc1.CareersLinkText"
        Then I expect that the url is "US.@tc1.CareersLinkUrl"
        And the title is "US.@tc24.CareersTitle"
        When I click on the link "<link>"
        Then I expect the url "<url>" is opened in a new tab

    Examples:
        | link                            | url                            |     
        | US.@tc25.1.EuropeLink           | US.@tc25.1.EuropeUrl           |
        | US.@tc25.1.AfricaLink           | US.@tc25.1.AfricaUrl           |
        | US.@tc25.1.AccessMyProfileLink  | US.@tc25.1.AccessMyProfileUrl  |
        | US.@tc25.1.MyJobSubmissionsLink | US.@tc25.1.MyJobSubmissionsUrl |

@tc25.2
Scenario: Validate that Careers page appears Music link 
        When I click on the link "US.@tc1.CareersLinkText"
        Then I expect that the url is "US.@tc1.CareersLinkUrl"
        And the title is "US.@tc24.CareersTitle"
        When I click on the link "US.@tc25.2.MusicLink"
        Then I expect that element "//div[@class='reverse-type' and contains(.,'US.@tc25.2.MusicText')]" is displayed

@tc26
Scenario: Validate that Careers page appears youtube video
        When I click on the link "US.@tc1.CareersLinkText"
        Then I expect that the url is "US.@tc1.CareersLinkUrl"
        When I click on the "//a[@href='#story5991']"
        Then I expect that video with id "//iframe[@id='yt_EmE_RWm9pNE']" is playing

@tc27
Scenario: Validate that Search page works correctly
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "tigris" to the inputfield "//input[@type='search']"
        And I click on the button "US.@tc27.SearchButton"
        Then I expect that element "body" contains the text "US.@tc27.SearchResultsTigris"
        And I expect that element "body" contains the text "US.@tc1.NewsEventsLinkText"
        And I expect that element "body" contains the text "US.@tc20.NewsEventsInputText5"
        And I expect that element "body" contains the text "US.@tc27.SearchInputText1"
        When I click on the link "US.@tc27.VascularStentLink"
        Then I expect that the url is "US.@tc27.VascularStentUrl"

  @tc28
  Scenario: Validate that Contact page appears texts and links
        When I click on the link "US.@tc1.ContactLinkText"
        Then I expect that the url is "US.@tc1.ContactLinkUrl"
        When I click on the link "US.@tc28.SeeMoreLink"
        Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
        When I click on the link "US.@tc1.ContactLinkText"
        Then I expect that element "body" contains the text "US.@tc28.ContactInputText"
        And I expect that element "body" contains the text "US.@tc28.ContactInputText1"
        And I expect that element "body" contains the text "US.@tc1.AboutGoreLinkText"
        And I expect that element "body" contains the text "US.@tc28.ContactInputText2"
        And I expect that element "body" contains the text "US.@tc28.ContactInputText3"
        When I click on the link "US.@tc28.ViewAllLocationsLink"
        Then I expect that the url is "US.@tc28.ViewAllLocationsUrl"

@tc29
Scenario: Validate that Consumer Products page appears texts and link
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.ConsumerProductsLinkText')]"
        Then I expect that the url is "US.@tc5.ConsumerProductsLinkUrl" 
        And I expect that element "body" contains the text "US.@tc5.ConsumerProductsLinkText"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText1"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText2"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText3"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText4"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText5"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText6"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText7"
        And I expect that element "body" contains the text "US.@tc29.ConsumerProductsInputText8"
        When I click on the link "US.@tc29.ViewAllIndustriesAndCategoriesLink"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        When I navigate back in browser
        And I click on the "//h4[contains(.,'US.@tc29.ConsumerProductsInputText7')]"
        Then I expect that the url is "US.@tc29.CleanstreamFiltersUrl"

@tc29.1
Scenario Outline: Validate that Consumer Products page appears links that opened a new tab
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.ConsumerProductsLinkText')]"
        Then I expect that the url is "US.@tc5.ConsumerProductsLinkUrl" 
        When I click on the link "<link>"
        Then I expect the url "<url>" is opened in a new tab
  
    Examples:
        | link                                 | url                                 |     
        | US.@tc29.1.VisitGoreTexComLink       | US.@tc29.1.VisitGoreTexComUrl       |
        | US.@tc29.1.VisitWindstopperComLink   | US.@tc29.1.VisitWindstopperComUrl   |
        | US.@tc29.1.VisitGoreWearComLink      | US.@tc29.1.VisitGoreWearComUrl      |
        | US.@tc29.1.VisitOptifadeComLink      | US.@tc29.1.VisitOptifadeComUrl      |
        | US.@tc29.1.VisitElixirstringsComLink | US.@tc29.1.VisitElixirstringsComUrl |


@tc30 @tc31
Scenario Outline: Validate that Consumer Products horizontal menu item <navItem> works correctly
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.ConsumerProductsLinkText')]"
        Then I expect that the url is "US.@tc5.ConsumerProductsLinkUrl" 
        When I click on the "//div[@class='sticky-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//div[@class='layout-5-3-of-8']//*[@data-section-title and contains(.,'<navItem>')]" is within the viewport

    Examples:
        | navItem                        | 
        | US.@tc30.OverviewMenuItem      | 
        | US.@tc30.GoreTexMenuItem       |
        | US.@tc30.WindstopperMenuItem   |
        | US.@tc30.GoreWearMenuItem      |
        | US.@tc30.OptifadeMenuItem      |
        | US.@tc30.ElixirStringsMenuItem |
        | US.@tc30.CleanstreamMenuItem   |

@tc32
Scenario Outline: Validate that Consumer Products page appears youtube <youtubeId> videos
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.ConsumerProductsLinkText')]"
        Then I expect that the url is "US.@tc5.ConsumerProductsLinkUrl" 
        When I click on the "//a[contains(@href,'<youtubeId>')]"
        Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
   
    Examples: 
        | youtubeId           |
        | US.@tc32.YoutubeId1 |
        | US.@tc32.YoutubeId2 |
        | US.@tc32.YoutubeId3 |
        | US.@tc32.YoutubeId4 |
        | US.@tc32.YoutubeId5 |

@tc33
Scenario: Validate that Cables page appears texts and link
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        Then I expect that the url is "US.@tc5.CablesLinkUrl" 
        And I expect that element "body" contains the text "US.@tc5.CablesLinkText"
        And I expect that element "body" contains the text "US.@tc33.CablesInputText1"
        And I expect that element "body" contains the text "US.@tc33.CablesOverviewMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesAerospaceMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesLandSystemsMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesSemiconductorMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesTestMeasurementMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesIndustrialAutomationMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesComputingNetworkingMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesInputText2"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText2"
        When I click on the link "US.@tc29.ViewAllIndustriesAndCategoriesLink"
        Then I expect that the url is "US.@tc1.ProductsLinkUrl"
        When I navigate back in browser
        And I click on the link "US.@tc33.TheTechnologyBehindtheCablesLink"
        Then I expect that element "//div[@class='inner longform cf']" is displayed
        
@tc37
Scenario Outline: Validate that Cables page appears <youtubeId> videos
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        Then I expect that the url is "US.@tc5.CablesLinkUrl" 
        When I click on the "//a[contains(@href,'<youtubeId>')]"
        Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
    
    Examples: 
        | youtubeId           | 
        | US.@tc37.YoutubeId1 |
        | US.@tc37.YoutubeId2 |

@tc38
Scenario: Validate that Tethered Drone Cables page appears texts
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesInputText1"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText2"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesInputText2"
        And I expect that element "body" contains the text "US.@tc1.ResourceLibraryLinkText"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesOverviewMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesRelatedProductsMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesRelatedIndustriesMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesApplicationsMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesBenefitsMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesPropertiesMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesResourcesMenuItem"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesRecentNewsMenuItem"

@tc38.1 
Scenario Outline: Validate that Tethered Drone Cables page appears <link> links
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        When I click on the link "<link>"
        Then I expect that the url is "<url>"

    Examples:
        | link                                     | url                                     |
        | US.@tc38.1.ContactByEmailLink            | US.@tc38.1.ContactByEmailUrl            |
        | US.@tc38.1.VideosLink                    | US.@tc38.1.VideosUrl                    |
        | US.@tc38.1.WhitePapersLink               | US.@tc38.1.WhitePapersUrl               |
        | US.@tc38.1.AuthorizedDistributorsLink    | US.@tc38.1.AuthorizedDistributorsUrl    |
        | US.@tc38.1.DataSheetsLink                | US.@tc38.1.DataSheetsUrl                |
        | US.@tc38.1.ShieldedTwistedPairCablesLink | US.@tc38.1.ShieldedTwistedPairCablesUrl |
        | US.@tc33.CablesAerospaceMenuItem         | US.@tc8.AerospaceUrl                    |
        | US.@tc38.1.GoreRepresentativeLink        | US.@tc38.1.GoreRepresentativeUrl        |
        | US.@tc38.1.ViewAllResourcesLink          | US.@tc38.1.ViewAllResourcesUrl          |
        | US.@tc38.1.ViewAllNewsEventsLink         | US.@tc38.1.ViewAllNewsEventsUrl         |
        | US.@tc38.1.GoreAerospaceLink             | US.@tc38.1.GoreAerospaceUrl             |  

@tc38 @tc39
Scenario Outline: Validate that Tethered Drone Cables <navItem> vertical menu and Back to top links works correctly
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//h4[contains(.,'<navItem>')]/../following-sibling::section[1]" is within the viewport
        When I click on the "//h4[contains(.,'<navItem>')]/../following-sibling::div[1]/footer//a"
        Then I expect that element "//h4[contains(.,'US.@tc38.TetheredDroneCablesOverviewMenuItem')]/../following-sibling::section[1]" is within the viewport

    Examples:
        | navItem                                          |
        | US.@tc38.TetheredDroneCablesOverviewMenuItem     |
        | US.@tc38.TetheredDroneCablesApplicationsMenuItem |
        | US.@tc38.TetheredDroneCablesBenefitsMenuItem     |
        | US.@tc38.TetheredDroneCablesPropertiesMenuItem   |
        | US.@tc38.1.VideosLink                            |

@tc38 @tc39.1
Scenario: Validate that Tethered Drone Cables Contact Us menu item and Back to top links works correctly
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'US.@tc22.AboutGoreInputText2')]"
        Then I expect that element "//h2[@id='section-contact' and contains(.,'US.@tc22.AboutGoreInputText2')]/../.." is within the viewport
        When I click on the "//h2[@id='section-contact' and contains(.,'US.@tc22.AboutGoreInputText2')]/../../following-sibling::footer//a"
        Then I expect that element "//h4[contains(.,'US.@tc38.TetheredDroneCablesOverviewMenuItem')]/../following-sibling::section[1]" is within the viewport

@tc39.2
Scenario: Validate that Tethered Drone Cables Resources and Recent News menu items works correctly
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'US.@tc38.TetheredDroneCablesResourcesMenuItem')]"
        Then I expect that element "//h4[contains(.,'US.@tc38.TetheredDroneCablesResourcesMenuItem')]/../following-sibling::div[1]" is within the viewport
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'US.@tc38.TetheredDroneCablesRecentNewsMenuItem')]"
        Then I expect that element "//h4[contains(.,'US.@tc38.TetheredDroneCablesRecentNewsMenuItem')]/../following-sibling::section[1]" is within the viewport

@tc40
Scenario: Validate that Tethered Drone Cables hotspot works correctly
        When I click on the link "US.@tc1.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
        And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
        Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'A')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText1')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'B')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText2')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'C')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText3')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'D')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText4')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'E')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText5')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'F')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText6')]" is displayed
        When I click on the "//div[contains(@class,'hotspots__spot')]/a[contains(.,'G')]"
        Then I expect that element "//div[contains(@class,'hotspots__caption')]//p[contains(.,'US.@tc40.CablesHotpostText7')]" is displayed

@tc42
Scenario: Validate that Article Resources page appears texts and links
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "Intermateability" to the inputfield "//input[@type='search']"
        And I click on the button "US.@tc27.SearchButton"
        And I click on the link "US.@tc42.ArticleResourcesLink"
        Then I expect that the url is "US.@tc42.ArticleResourcesUrl"
        Then I expect that element "body" contains the text "US.@tc1.ResourceLibraryLinkText"
        And I expect that element "body" contains the text "US.@tc42.ArticleResourcesInputText1"
        And I expect that element "body" contains the text "US.@tc42.ArticleResourcesInputText2"
        And I expect that element "body" contains the text "US.@tc42.ArticleResourcesInputText3"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText2"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesRelatedProductsMenuItem"
        And I expect that element "body" contains the text "US.@tc33.CablesInputText2"
        When I click on the link "US.@tc38.1.ContactByEmailLink"
        Then I expect that the url is "US.@tc42.ContactByEmailUrl"
        When I navigate back in browser
        And I click on the link "US.@tc42.HighFrequencyCableLink"
        Then I expect that the url is "US.@tc42.HighFrequencyCableUrl"
        When I navigate back in browser
        And I click on the link "US.@tc8.AerospaceIndustry"
        Then I expect that the url is "US.@tc8.AerospaceUrl"

@tc44
Scenario: Validate that Microwave RF Cables Assemblies page appears texts and links
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "Microwave/RF Cable Assemblies" to the inputfield "//input[@type='search']"
        And I click on the button "US.@tc27.SearchButton"
        And I click on the link "US.@tc44.MicrowaveCableAssembliesLink"
        Then I expect that the url is "US.@tc44.MicrowaveCableAssembliesUrl"
        Then I expect that element "body" contains the text "US.@tc1.NewsEventsLinkText"
        And I expect that element "body" contains the text "US.@tc44.MicrowaveCableAssembliesLink"
        And I expect that element "body" contains the text "US.@tc44.MicrowaveCableAssembliesTextInput1"
        And I expect that element "body" contains the text "US.@tc22.AboutGoreInputText2"
        And I expect that element "body" contains the text "US.@tc38.TetheredDroneCablesRelatedProductsMenuItem"
        And I expect that element "body" contains the text "US.@tc44.MicrowaveCableAssembliesTextInput2"
        And I expect that element "body" contains the text "US.@tc44.MicrowaveCableAssembliesTextInput3"
        And I expect that element "body" contains the text "US.@tc44.MicrowaveCableAssembliesTextInput4"
        When I click on the link "US.@tc44.GoreFlightMicrowaveAssembliesLink"
        Then I expect that the url is "US.@tc44.GoreFlightMicrowaveAssembliesUrl"
        When I navigate back in browser
        And I click on the link "US.@tc44.WhitePaperLink"
        Then I expect that the url is "US.@tc44.WhitePaperUrl"
        When I navigate back in browser
        And I click on the link "US.@tc8.AerospaceIndustry"
        Then I expect that the url is "US.@tc8.AerospaceUrl"
        When I navigate back in browser
        And I click on the link "US.@tc44.ViewAllIndustriesLink"
        Then I expect that the url is "US.@tc44.ViewAllIndustriesUrl"