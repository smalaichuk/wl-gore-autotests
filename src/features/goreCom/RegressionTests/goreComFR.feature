Feature: goreComFR
   Check gore.com main page functionality with FR location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'France')]"
        Then I expect that the url is "gore.fr"

@tc16
Scenario: Validate that the filtering by "Language" works correctly
        When I click on the link "FR.@tc16.ResourcesLink"
        And I click on the "//input[@value='language:fr']"
        And I click on the "//input[@value='language:es']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'FR.@tc16.ResourceSpanishFilter')]" is displayed
        When I click on the button "FR.@tc16.ViewResultsLink"
        Then I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText1"
        And I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText2"
        And I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText3"
        And I expect that element "//div[@class='tabs']//a[contains(.,'FR.@tc16.LanguageTab')]/span[contains(@class,'tab-active')]" is displayed

@tc28
Scenario: Validate that Contact page appears texts and links
        When I click on the link "US.@tc1.ContactLinkText"
        Then I expect that the url is "US.@tc1.ContactLinkUrl"
        When I click on the link "FR.@tc28.SeeMoreLink"
        Then I expect that the url is "FR.@tc28.AboutGoreUrl"
        When I click on the link "US.@tc1.ContactLinkText"
        Then I expect that element "body" contains the text "FR.@tc28.ContactInputText"
        And I expect that element "body" contains the text "FR.@tc28.ContactInputText1"
        And I expect that element "body" contains the text "FR.@tc28.ContactInputText2"
        And I expect that element "body" contains the text "FR.@tc28.ContactInputText3"
        When I click on the link "FR.@tc28.ViewAllLocationsLink"
        Then I expect that the url is "FR.@tc28.ViewAllLocationsUrl"