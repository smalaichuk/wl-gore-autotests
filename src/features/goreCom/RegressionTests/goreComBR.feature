Feature: goreComBR
    Check gore.com main page functionality with BR location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'Brasil')]"
        Then I expect that the url is "gore.com.br"
         
@tc18
Scenario: Validate that the filtering by "Categories" works correctly
        When I click on the link "BR.@tc18.ResourceLibraryLink"
        And I click on the "//legend/..//a[contains(.,'BR.@tc18.FibresLink')]"
        And I click on the "//legend/..//input[@value='categories:791']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'BR.@tc18.AerospaceInputText1')]" is displayed
        When I click on the button "BR.@tc18.ViewResultsLink"
        Then I expect that element "body" contains the text "BR.@tc18.AerospaceInputText1"
        And I expect that element "body" contains the text "BR.@tc18.AerospaceInputText2"
        And I expect that element "body" contains the text "BR.@tc18.AerospaceInputText3"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'BR.@tc18.CategoriesTab')]/span[contains(@class,'tab-active')]" is displayed

@tc30 @tc31
Scenario Outline: Validate that Consumer Products horizontal menu item <navItem> works correctly
        When I click on the link "BR.@tc30.ProductsLink"
        And I click on the "//h3/a[contains(text(),'BR.@tc30.ConsumerProductsLink')]"
        Then I expect that the url is "BR.@tc30.ConsumerProductsUrl" 
        When I click on the "//div[@class='sticky-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//div[@class='layout-5-3-of-8']//*[@data-section-title and contains(.,'<navItem>')]" is within the viewport

    Examples:
        | navItem                        | 
        | BR.@tc30.OverviewMenuItem      | 
        | BR.@tc30.GoreTexMenuItem       |
        | BR.@tc30.WindstopperMenuItem   |
        | US.@tc30.GoreWearMenuItem      |
        | US.@tc30.OptifadeMenuItem      |
        | US.@tc30.ElixirStringsMenuItem |