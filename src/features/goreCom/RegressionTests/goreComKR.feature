Feature: goreComKR 
    Check gore.com main page functionality with KR location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'한국')]"
        Then I expect that the url is "kr.gore.com"

@tc20
Scenario: Validate that News & Events page appears texts
        When I click on the link "KR.@tc20.NewsEventsLink"
        Then I expect that the url is "KR.@tc20.NewsEventsUrl"
        And the title is "KR.@tc20.NewsEventsTitle"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsLink"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText1"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText2"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText3"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText4"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText5"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText6"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText7"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText8"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText9"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText10"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText11"
        And I expect that element "body" contains the text "KR.@tc20.NewsEventsInputText12"

@tc39
Scenario Outline: Validate that High Flex Flat Cable <navItem> vertical menu and Back to top links works correctly
        When I click on the link "KR.@tc39.ProductsLink"
        And I click on the "//h3/a[contains(text(),'KR.@tc39.CablesLink')]"
        And I click on the "//h4[contains(text(),'KR.@tc39.HighFlexFlatCableLink')]"
        Then I expect that the url is "KR.@tc39.HighFlexFlatCableUrl"
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//h4[contains(.,'<navItem>')]/../following-sibling::section[1]" is within the viewport
        
    Examples:
        | navItem                          |
        | KR.@tc39.OverviewLink            |
        | KR.@tc39.ApplicationsLink        |
        | KR.@tc39.FeaturesAndBenefitsLink |

@tc39.1
Scenario Outline: Validate that High Flex Flat Cable <navItem> vertical menu and Back to top links works correctly
        When I click on the link "KR.@tc39.ProductsLink"
        And I click on the "//h3/a[contains(text(),'KR.@tc39.CablesLink')]"
        And I click on the "//h4[contains(text(),'KR.@tc39.HighFlexFlatCableLink')]"
        Then I expect that the url is "KR.@tc39.HighFlexFlatCableUrl"
        When I click on the "//ul[@class='sticky-side-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//*[contains(.,'<navItem>') and contains(@id, 'section')]/../following-sibling::div[1]" is within the viewport
        
    Examples:
        | navItem                |
        | KR.@tc39.ResourcesLink |
        | KR.@tc39.ContactUsLink |