Feature: goreComVRTFullPages 
    Visual testing of gore.com full pages for locations

    Background: Open GoreCom
        Given I open "HomePage" page
        Then I expect that the url is "US.goreComLink"

    @tc1 @visual
    Scenario: Check main page for US location
        Then I expect that the full page screenshot 'goreComMainPageUS' is equal to baseline

    @tc2 @visual
    Scenario Outline: Check main page for <location> location
        When I click on the "//select[@id='region-select']/option[contains(.,'<location>')]"
        Then I expect that the url is "<url>"
        And I expect that the full page screenshot '<baseline>' is equal to baseline 

        Examples:
            | location                | url                    | baseline          |
            | UK.@vrt.goreComTextLink | UK.@vrt.goreComLinkUrl | goreComMainPageUK |
            | DE.@vrt.goreComTextLink | DE.@vrt.goreComLinkUrl | goreComMainPageDE |
            | ES.@vrt.goreComTextLink | ES.@vrt.goreComLinkUrl | goreComMainPageES |
            | FR.@vrt.goreComTextLink | FR.@vrt.goreComLinkUrl | goreComMainPageFR |
            | IT.@vrt.goreComTextLink | IT.@vrt.goreComLinkUrl | goreComMainPageIT |
            | BR.@vrt.goreComTextLink | BR.@vrt.goreComLinkUrl | goreComMainPageBR |
            | KR.@vrt.goreComTextLink | KR.@vrt.goreComLinkUrl | goreComMainPageKR |
            | JA.@vrt.goreComTextLink | JA.@vrt.goreComLinkUrl | goreComMainPageJA |
            | CH.@vrt.goreComTextLink | CH.@vrt.goreComLinkUrl | goreComMainPageCH |