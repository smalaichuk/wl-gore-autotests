Feature: goreComJA 
    Check gore.com main page functionality with JA location

    Background: Open GoreCom
        Given I open "HomePage" page
        When I click on the "//select[@id='region-select']/option[contains(.,'日本')]"
        Then I expect that the url is "gore.co.jp"

@tc19
Scenario: Validate that the filtering by "Industries" works correctly
        When I click on the link "JA.@tc18.ResourceLibraryLink"
        And I click on the "//input[@value='industries:666']"
        Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'JA.@tc18.ApparelTextilesIndustry')]" is displayed
        When I click on the button "JA.@tc18.ViewResultsLink"
        Then I expect that element "body" contains the text "JA.@tc19.IndustriesInputText1"
        And I expect that element "body" contains the text "JA.@tc19.IndustriesInputText2"
        And I expect that element "body" contains the text "JA.@tc19.IndustriesInputText3"   
        And I expect that element "//div[@class='tabs']//a[contains(.,'JA.@tc18.IndustriesTab')]/span[contains(@class,'tab-active')]" is displayed

@tc35 @tc36
Scenario Outline: Validate that Cables horizontal menu item <navItem> works correctly
        When I click on the link "JA.@tc35.ProductsLinkText"
        And I click on the "//h3/a[contains(text(),'JA.@tc35.CablesLinkText')]"
        Then I expect that the url is "US.@tc5.CablesLinkUrl" 
        When I click on the "//div[@class='sticky-nav']//a[contains(.,'<navItem>')]"
        Then I expect that element "//div[@class='layout-5-3-of-8']//*[@data-section-title and contains(.,'<navItem>')]" is within the viewport

    Examples:
        | navItem                                     | 
        | JA.@tc35.CablesOverviewMenuItem             |
        | JA.@tc35.CablesAerospaceMenuItem            |
        | JA.@tc35.CablesLandSystemsMenuItem          |
        | JA.@tc35.CablesSemiconductorMenuItem        |
        | JA.@tc35.CablesTestMeasurementMenuItem      |
        | JA.@tc35.CablesIndustrialAutomationMenuItem |
        | JA.@tc35.CablesComputingNetworkingMenuItem  |