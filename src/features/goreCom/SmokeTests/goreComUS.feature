Feature: goreComUS 
    Check gore.com functionality with US location 

    Background: Open GoreCom
        Given I open "HomePage" page
        Then I expect that the url is "gore.com"

# @tc1 @tc2
# Scenario Outline: Validate that <link> link opens correctly
#         When I click on the link "<link>"
#         Then I expect that the url is "<url>"
#         And I expect that element "body" contains the text "<inputText>"
         
#      Examples:
#         | link                            | url                            | inputText                        |
#         | US.@tc1.ContactLinkText         | US.@tc1.ContactLinkUrl         | US.@tc1.ContactInputText         |
#         | US.@tc1.ProductsLinkText        | US.@tc1.ProductsLinkUrl        | US.@tc1.ProductsInputText        |
#         | US.@tc1.ResourceLibraryLinkText | US.@tc1.ResourceLibraryLinkUrl | US.@tc1.ResourceLibraryInputText |
#         | US.@tc1.NewsEventsLinkText      | US.@tc1.NewsEventsLinkUrl      | US.@tc1.NewsEventsInputText      |
#         | US.@tc1.AboutGoreLinkText       | US.@tc1.AboutGoreLinkUrl       | US.@tc1.AboutGoreInputText       |
#         | US.@tc1.CareersLinkText         | US.@tc1.CareersLinkUrl         | US.@tc1.CareersInputText         |
#         | US.@tc1.PrivacyNoticeLinkText   | US.@tc1.PrivacyNoticeLinkUrl   | US.@tc1.PrivacyNoticeInputText   |
#         | US.@tc1.TermsOfUseLinkText      | US.@tc1.TermsOfUseLinkUrl      | US.@tc1.TermsOfUseInputText      |
#         | US.@tc1.CaliforniaSCALinkText   | US.@tc1.CaliforniaSCALinkUrl   | US.@tc1.CaliforniaSCAInputText   |

# @tc2.1
# Scenario: Validate that Youtube link opens correctly
#         When I click on the link "YouTube"
#         Then I expect the url "youtube.com/user/wlgoreassociates" is opened in a new tab

# @tc4
# Scenario Outline: Validate that gore main page appears <youtubeId> videos
#         When I click on the "//a[contains(@href,'<youtubeId>')]"
#         Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
    
#     Examples: 
#         | youtubeId          |
#         | US.@tc4.YoutubeId1 |
#         | US.@tc4.YoutubeId2 |

# @tc6 @tc7
# Scenario: Validate that main page appears texts and links
#         When I click on the link "US.@tc6.MoreAboutGore"
#         Then I expect that the url is "US.@tc1.AboutGoreLinkUrl"
#         And the title is "US.@tc6.AboutGoreTitle"
#         When I open "HomePage" page
#         And I click on the link "US.@tc6.MoreTechnologies"
#         Then I expect that the url is "US.@tc6.MoreTechnologiesUrl"
#         And the title is "US.@tc6.MoreTechnologiesTitle"
#         When I open "HomePage" page
#         And I click on the link "US.@tc6.ExploreTechnologies"
#         Then I expect that the url is "US.@tc6.ExploreTechnologiesUrl"
#         And the title is "US.@tc6.ExploreTechnologiesTitle"
#         When I open "HomePage" page
#         Then I expect that element "body" contains the text "US.@tc6.InputText1"
#         And I expect that element "body" contains the text "US.@tc6.InputText2"
#         And I expect that element "body" contains the text "US.@tc6.InputText3"
#         And I expect that element "body" contains the text "US.@tc6.InputText4"

# @tc9
# Scenario: Validate that Product page appears texts and links
#         When I click on the link "US.@tc1.ProductsLinkText"
#         Then I expect that the url is "US.@tc1.ProductsLinkUrl"
#         And the title is "US.@tc9.ProductsTitle"
#         And I expect that element "body" contains the text "US.@tc9.ProductsInputText1"
#         And I expect that element "body" contains the text "US.@tc9.ProductsInputText2"
#         And I expect that element "body" contains the text "US.@tc1.ProductsLinkText"
#         And I expect that element "body" contains the text "US.@tc9.ProductsInputText3"

# @tc16
# Scenario: Validate that the filtering by "Language" works correctly
#         When I click on the link "US.@tc1.ResourceLibraryLinkText"
#         And I click on the "//input[@value='language:en']"
#         And I click on the "//input[@value='language:es']"
#         Then I expect that element "//div[@class='sticky-filters']//a[contains(.,'US.@tc16.ResourceSpanishFilter')]" is displayed
#         When I click on the button "US.@tc13.ViewResultsButton"
#         Then I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText1"
#         And I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText2"
#         And I expect that element "body" contains the text "ES.@tc16.ResourceViewResultsInputText3"
#         And I expect that element "//div[@class='tabs']//a[contains(.,'US.@tc12.ResourceLibraryInputText3')]/span[contains(@class,'tab-active')]" is displayed

# @tc34
# Scenario Outline: Validate that Cables page appears <menuItem> vertical menu pt.1
#         When I click on the link "US.@tc1.ProductsLinkText"
#         And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
#         Then I expect that the url is "US.@tc5.CablesLinkUrl" 
#         When I click on the "//header[contains(@class,'accordion__header')]//a[contains(.,'<menuItem>')]"
#         Then I expect that element "//a[contains(.,'<menuItem>')]/../../../div[contains(@class,'subcategory-accordion')]" is displayed 

#     Examples:
#         | menuItem                              |
#         | US.@tc34.DefenseAircraftMenuItem      |
#         | US.@tc34.CivilAircraftMenuItem        |
#         | US.@tc34.SpaceflightMenuItem          |
#         | US.@tc34.IndustrialAutomationMenuItem |
#         | US.@tc34.HarshEnvironmentsMenuItem    | 

# @tc34.1
# Scenario Outline: Validate that Cables page appears vertical menu <menuItem> pt.2
#         When I click on the link "US.@tc1.ProductsLinkText"
#         And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
#         Then I expect that the url is "US.@tc5.CablesLinkUrl" 
#         When I click on the "//header[contains(@class,'accordion__header')]//a[contains(.,'<menuItem>')]"
#         Then I expect that element "//header[contains(@class,'accordion__header')]//a[contains(.,'<menuItem>')]/../../../div[contains(@class,'accordion__body')]" is not displayed 
#         When I click on the "//header[contains(@class,'accordion__header')]//a[contains(.,'<menuItem>')]"
#         Then I expect that element "//header[contains(@class,'accordion__header')]//a[contains(.,'<menuItem>')]/../../../div[contains(@class,'accordion__body')]" is displayed 

#     Examples:
#         | menuItem                                   |
#         | US.@tc34.1.DefenseLandSystemsMenuItem      |
#         | US.@tc33.CablesSemiconductorMenuItem       |
#         | US.@tc34.1.TestMenuItem                    |
#         | US.@tc33.CablesComputingNetworkingMenuItem | 

# @tc35 @tc36
# Scenario Outline: Validate that Cables horizontal menu item <navItem> works correctly
#         When I click on the link "US.@tc1.ProductsLinkText"
#         And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
#         Then I expect that the url is "US.@tc5.CablesLinkUrl" 
#         When I click on the "//div[@class='sticky-nav']//a[contains(.,'<navItem>')]"
#         Then I expect that element "//div[@class='layout-5-3-of-8']//*[@data-section-title and contains(.,'<navItem>')]" is within the viewport

#     Examples:
#         | navItem                                     | 
#         | US.@tc33.CablesOverviewMenuItem             |
#         | US.@tc33.CablesAerospaceMenuItem            |
#         | US.@tc33.CablesLandSystemsMenuItem          |
#         | US.@tc33.CablesSemiconductorMenuItem        |
#         | US.@tc33.CablesTestMeasurementMenuItem      |
#         | US.@tc33.CablesIndustrialAutomationMenuItem |
#         | US.@tc33.CablesComputingNetworkingMenuItem  |


# @tc41
# Scenario Outline: Validate that Tethered Drone Cables page appears <youtubeId> videos
#         When I click on the link "US.@tc1.ProductsLinkText"
#         And I click on the "//h3/a[contains(text(),'US.@tc5.CablesLinkText')]"
#         And I click on the "//h4[contains(text(),'US.@tc38.TetheredDroneCablesLink')]"
#         Then I expect that the url is "US.@tc38.TetheredDroneCablesUrl"
#         When I click on the "//a[contains(@href,'<youtubeId>')]"
#         Then I expect that video with id "//iframe[@id='yt_<youtubeId>']" is playing
    
#     Examples: 
#         | youtubeId           |
#         | US.@tc41.YoutubeId1 |
#         | US.@tc41.YoutubeId2 |
#         | US.@tc41.YoutubeId3 |

@tc43
Scenario: Validate that User can donwload file on Article Resources page
        When I click on the "//li[contains(@class,'search-item')]"
        And I set "Intermateability" to the inputfield "//input[@type='search']"
        And I click on the button "US.@tc27.SearchButton"
        And I click on the link "US.@tc42.ArticleResourcesLink"
        Then I expect that the url is "US.@tc42.ArticleResourcesUrl"
        When I click on the "//a[@class='button is-local' and contains(@href, 'US.@tc43.PdfFileHref')]"
        Then File "//a[@class='button is-local' and contains(@href, 'US.@tc43.PdfFileHref')]" is downloaded and include "US.@tc43.PdfTextInput" text

# @tc45
# Scenario: Validate that Microwave RF Cables Assemblies accordion wotks correctly
#         When I click on the "//li[contains(@class,'search-item')]"
#         And I set "Microwave/RF Cable Assemblies" to the inputfield "//input[@type='search']"
#         And I click on the button "US.@tc27.SearchButton"
#         And I click on the link "US.@tc44.MicrowaveCableAssembliesLink"
#         Then I expect that the url is "US.@tc44.MicrowaveCableAssembliesUrl"
#         Then I expect that element "body" contains the text "US.@tc1.NewsEventsLinkText"
#         When I click on the "//p/a[contains(.,'US.@tc45.MicrowaveCableQuestion1')]"
#         Then I expect that element "//div[@class='accordion__body']//p[contains(.,'US.@tc45.MicrowaveCableAnswer1')]" is displayed 
#         When I click on the "//p/a[contains(.,'US.@tc45.MicrowaveCableQuestion2')]"
#         Then I expect that element "//div[@class='accordion__body']//p[contains(.,'US.@tc45.MicrowaveCableAnswer2')]" is displayed  