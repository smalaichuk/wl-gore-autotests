# WL GORE TAF Solution

### Project structure
```
.
├── README.md ............................................................. This file
├── package.json .......................................................... Dependencies file
├── src
│   ├── features .......................................................... Test features 
│   │        
│   ├── steps ............................................................. Step definitions for scenarios steps
│   │   ├── goreCom ....................................................... Specific step definitions for gore.com site 
│   │   └── goreMedicalCom ................................................ Specific step definitions for goremedical.com site 
│   └── support ........................................................... Project step definitions helpers code
│       │   
│       └── pageObjects ................................................... Page objects for goremedical.com and gore.com
├── test
│   └── support ........................................................... Helpers for actions and checkers
│       ├── action
│       └── check
├── vrt ................................................................... Visual regression app code
│           
├── wdio.conf.js .......................................................... Main config file
├── wdio.bs.conf.js ....................................................... Specific BrowserStack config file
├── wdio.cbt.conf.js ...................................................... Specific CrossBrowserTesting config file
├── wdio.sl.conf.js ....................................................... Specific Saucelab config file
├── wdio.ggr.conf.js ...................................................... Specific Ggr config file
├── wdio.gorecom.conf.js ...................................................Specific gore.com config file
├── wdio.goremedicalcom.conf.js ........................................... Specific goremedical.com config file
└── wdio.local.conf.js .................................................... Specific config file for local testing
```

### Installation

* Install node.js v12.x and npm
    > `https://docs.npmjs.com/downloading-and-installing-node-js-and-npm`
  > (!) For windows users: after install node.js you should install Python3.8 because this is necessary for node `node-gyp` package.
  > To install the python package, ensure you have the latest Windows 10 updates and search the Microsoft Store app for “Python 3.8”. Ensure that the app you select is published by the Python Software Foundation, and install it.
  > after this you should install windows build tool: `npm install --global --production windows-build-tools` using 'Run As Administrator' cli  
  > or use Install Visual C++ Build Environment: [Visual Studio Build Tools](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools) (using "Visual C++ build tools" workload) 
  > or Visual Studio 2017 Community (using the "Desktop development with C++" workload) and then Launch cmd, `npm config set msvs_version 2017`

* Install git
    > https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
* Download and install Google Chrome
    > https://www.google.com/chrome/?brand=CHBD&gclid=CjwKCAiAws7uBRAkEiwAMlbZjh7tWdjltDpU4Vru_5LsbOlVCkW0v9hU2v2X-mdU5oBu5hLU1sEbmBoCfgcQAvD_BwE&gclsrc=aw.ds
* Download and install Cromedriver
    >  https://chromedriver.chromium.org/getting-started 
* Clone the project:
    >  git clone  git@github.corp.globant.com:viktar-silakou/temp-mvp.git
* Goto project directory and run npm installation
    >  cd 'project folder'
                                                    
    >  npm install

If you want to use image comparison you can use Applitools adapter or setup local VRT image comparison.

To install local VRT image comparison 

* Install and start mongoDB https://docs.mongodb.com/manual/installation/

* Create symlink for screenshots:
    
    > `mklink /D 'absolute path to project'\vrt\snapshoots 'absolute path to project'\tests\BaselineImages
 
* Goto project vrt directory, run npm installation and 
     >  cd vrt
                                                     
     >  npm install
                                                                                                                               
If you get some errors related `node-canvas` module during `npm install` please read manual [installation document](https://github.com/Automattic/node-canvas/wiki/Installation:-Windows)                                                                                                                               
* Start vrt services                                                                                                                                         
     >  npm run start;
                       
* To clear DB 
on macOs/Linux:
     >  npm run clear;    
                                
on Windows:
     >  npm run clearwin;    

### Test running
on Linux and macOs:
```$xslt
export LOCALE=<locale>
export PROVIDER=<environment_name>
export APP=<appname>
npx npm run test <feature_path>
```
`LOCALE=<locale> PROVIDER=<environment_name> APP=<appname> npx npm run test <feature_path>`
on Windows: 
```$xslt
set LOCALE=<locale>
set PROVIDER=<environment_name>
set APP=<appname>
npx npm run testwin <feature_path>
``` 
or

```$xslt
set APP=<appname>&set PROVIDER=<environment_name>&set LOCALE=<locale>& npx npm run testwin <feature_path>
```
* environment_name - which environment use, related to provider, options: `local`, `selenoid`, `sl`, `cbt`, `bs`;
* feature_path - path to the feature file;

Example Linux/macOs:
  `PROVIDER=local APP=goremedicalcom npx npm run test src/features/GORE_DEMO/search_outline.feature`
Example Windows:
   ```$xslt
set LOCALE=en 
set PROVIDER=local
set APP=goremedicalcom
npx npm run testwin src\features\goreMedicalCom\warmUpPo\DemoHomePage.feature
```

```set APP=goremedicalcom&set PROVIDER=local& npx npm run testwin src\features\goreMedicalCom\warmUpPo\DemoHomePage.feature```

### Test running in parallel

```
on Linux and macOs:

`PROVIDER=<environment_name> APP=<appname> FEATURE='<path_to_feature>' npx npm run parallel`

on Windows: 

`set APP=<appname> PROVIDER=<environment_name> FEATURE='<path_to_feature>' npx npm run parallelwin`
``` 

Examples:

```
on Linux and macOs:

`PROVIDER=bs APP=gorecom FEATURE='src/features/goreCom/SmokeTests/goreHomePageUS.feature' npx npm run parallel`

on Windows: 

`set APP=gorecom&set PROVIDER=bs&set FEATURE=src/features/goreCom/SmokeTests/goreComVRT.feature& npx npm run parallelwin`
``` 

### Reporting
To run report after test running:

`allure serve tests/reports`
                     
### Advanced Configuration

### Using Applitools

To use Applitools SaaS you need set `APPLITOOLS_API_KEY` environment variable and set `ATDriver` as VRTDriver in certain config file (by default it)

Example for macOs and Linux:
> `export APPLITOOLS_API_KEY=123`

Example for Windows:
> `set APPLITOOLS_API_KEY=123`

### Using GGR

Sometimes it is more convenient to use Go Grid Router (https://github.com/aerokube/ggr) to run browsers on any mixed environments,
for example you can simulteniosly run these tests in: local env, BrowserStack, Selenoid, etc.

Order of operations:

0. Install docker on host
1. Create ggr folder:

    `mkdir -p /etc/grid-router/quota`
2. Create users.htpasswd file:

   `$ htpasswd -bc /etc/grid-router/users.htpasswd test test-password`
3. Start Selenium standalone server on port 4445 (or other, it depends of nodes design):
   
   `$ java -jar selenium-server-standalone.jar -port 4445`
   
4. Create and edit router data file (use correct browser name and version):
`/etc/grid-router/quota/test.xml`

file example:
```
<qa:browsers xmlns:qa="urn:config.gridrouter.qatools.ru">
<browser name="firefox" defaultVersion="latest">
    <version number="latest">
        <region name="1">
            <host name="host.docker.internal" port="4445" count="1"/>
        </region>
    </version>
</browser>
<browser name="chrome" defaultVersion="latest">
    <version number="latest">
        <region name="1">
            <host name="host.docker.internal" port="4445" count="1"/>
        </region>
    </version>
</browser>
<browser name="safari" defaultVersion="latest">
    <version number="latest">
        <region name="1">
            <host name="host.docker.internal" port="4445" count="1"/>
        </region>
    </version>
</browser>
<browser name="internet explorer" defaultVersion="latest">
    <version number="latest">
        <region name="1">
            <host name="host.docker.internal" port="4449" count="1"/>
        </region>
    </version>
</browser>
</qa:browsers>
```

**Note:** file name should correspond to user name you added to `htpasswd` file. For user test we added on previous steps you should create z.
   
5. Install GGR docker container:

```
docker run -p 4444:4444 -d --name \
    ggr -v /private/etc/grid-router/:/etc/grid-router:ro \
     aerokube/ggr:latest-release
```
After setup you can check accessibility of ggr service using url:

`http://localhost:4444/ping`

Also yo can check if the service works properly using wdio REPL:

`wdio repl -h localhost -p 4449 'internet explorer'`
4. Change settings in corresponding configuration file: host, port, capabilities, etc.

### GGR troubleshooting 

Restart:
`docker stop ggr && docker start ggr`

Inspect:

`docker inspect ggr`

`docker inspect -f "{{.State.Status}}" ggr`
    
### Localization

There is possibility to localize each test, for this it is needs:
  * Setup common localization file for particular language in  `src/lib` folder, e.g.: `src/lib/localization.js`;
  * Setup specific localization file for certain feature if it's need: `<feature path>.locale.json` e.g.: `src/features/GORE_DEMO/debug.feature.locale.json`;
  * Run test with particular locales variable, for example: `LOCALE=de`;


