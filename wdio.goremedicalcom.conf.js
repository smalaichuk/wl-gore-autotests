const PROVIDER = process.env.PROVIDER;
const { config } = require(`./wdio.${PROVIDER}.conf.js`);

config.cucumberOpts.require.push('./src/steps/goreMedicalCom/*.js');
config.baseUrl = 'https://www.goremedical.com/';

exports.config = config;
